"""Stav Grossfeld - copaste copies a given file's absolute
path to a specified directory that already exists"""


import os


def copaste(file_path, folder_path):
    """receives file path and copies to a given folder."""

    in_file = file_path.lstrip().rstrip()

    path = folder_path.lstrip().rstrip()
    out_folder = path + '/' + in_file.split('/')[-1]

    assert (os.path.isfile(str(in_file)))
    assert (os.path.isdir(path))

    with open(in_file, "r") as in_file:
        with open(out_folder, "w") as out_file:
            out_file.write(in_file.read())


def main():
    """This function calls the copy function."""

    copaste("/Users/Stav/Desktop/git_screen.jpg", "/Users/Stav/Desktop"
                                                  "/git_screen_folder")

if __name__ == '__main__':
    main()
