"""Stav Grossfeld - counts frequency of words in an input file, and prints
the top 20/ all words sorted"""

import sys
import string
import re
import os

THRESHOLD = 20
OPTION_ARG = 1
FILENAME_ARG = 2
WRONG_ARG = 3


def utilities(filename):
    my_file = open(filename, 'r').read()

    my_file = my_file.lower()

    words = re.findall("[\w']+", my_file)
    word_dict = {}

    for word in words:
        if word not in word_dict:
            word_dict[word] = 1
        else:
            word_dict[word] = word_dict[word] + 1

    del word_dict["'"]
    return word_dict


def print_top(filename):
    """calls utilities function, saves dictionary to word_dict and prints
      the sorted word_count dictionary if the count < 20"""

    word_dict = utilities(filename)

    limited_word_dict = {}
    top_word_ct = 0
    for key, value in sorted(word_dict.iteritems(), key=lambda (k, v): (v,
                                                                        k),
                             reverse=True):
        top_word_ct = top_word_ct + 1
        if top_word_ct <= THRESHOLD:
            limited_word_dict[key] = value

    for key in sorted(limited_word_dict):
        print key, ":", limited_word_dict[key]


def print_words(word_dict):
    """calls utilities function, saves dictionary to word_dict and prints
    the sorted word_count dictionary"""

    word_dict = utilities(word_dict)

    for key in sorted(word_dict):
        print key, ":", word_dict[key]


def make_sure_file_exists(filename):
    if os.path.isfile(filename):
        return True
    else:
        print 'incorrect file'
        sys.exit(1)


def main():
    """checks for valid system arguements, and calls functions based on the
     user input"""

    if len(sys.argv) != WRONG_ARG:
        print 'usage: ./wordcount.py {--count | --topcount} file'

    option = sys.argv[OPTION_ARG]
    filename = sys.argv[FILENAME_ARG]

    make_sure_file_exists(filename)

    if option == '--count':
        print_words(filename)
    elif option == '--topcount':
        print_top(filename)
    else:
        print 'unknown option: ' + option
        sys.exit(1)


if __name__ == '__main__':
    main()
