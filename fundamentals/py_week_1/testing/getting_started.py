# -*- coding: utf-8 -*-
import urllib2
from sys import argv

from BeautifulSoup import BeautifulSoup


def wget(url):
    req = urllib2.Request(url)
    req.add_header("User-Agent", "Mozilla/5.0 (Compatible)")
    return urllib2.urlopen(req).read()


def wikisnip(html):
    soup = BeautifulSoup(html)

    mwContentText = soup.find('div', {'id': 'mw-content-text'})
    mwParserOutput = mwContentText.find('div', {'class': 'mw-parser-output'})
    return mwParserOutput.findAll('p', recursive=False)[0].getText(' ').encode('utf-8')


def get_term_url(term):
    return 'https://en.wikipedia.org/wiki/%s' % term


def extract_from_wiki(term):
    assert term, 'non empty term must be provided'
    url = get_term_url(term)
    html = wget(url)
    return wikisnip(html)


def test_extract_from_wiki():
    assert extract_from_wiki("iphone") == "iPhone ( / ˈ aɪ f oʊ n /  EYE " \
                                          "-fohn ) is a line of smartphones designed and marketed by Apple Inc. They run Apple's iOS mobile operating system. The first generation iPhone was released on June 29, 2007, and there have been multiple new hardware iterations with new iOS releases since."

def main():
    term = ' '.join(argv[1:])
    test_extract_from_wiki()

main()