"""Stav Grossfeld - program takes a list of numbers
delimited by spaces and returns the product"""


def multiplier():
    """This function asks the user to provide
    a list of numbers delimited by spaces.
    Then, prints the product of all numbers. """

    my_numbers = raw_input("enter numbers delimited by space: ")

    product = 1

    for number in my_numbers.split():
        product = product * int(number)

    return int(product)


def main():

    """ This function calls the multiplier function and prints the output."""

    print multiplier()

if __name__ == '__main__':
    main()
