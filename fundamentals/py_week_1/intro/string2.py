""" Stav Grossfeld - String manipulations"""

VERBING_THRESHOLD = 3
PUNCTUATION_CONST = -1
# E. verbing
# Given a string, if its length is at least 3,
# add 'ing' to its end.
# Unless it already ends in 'ing', in which case
# add 'ly' instead.
# If the string length is less than 3, leave it unchanged.
# Return the resulting string.
def verbing(my_inputs):
    """  if length of a string is 3 add ing to its end unless its already
    ending in ing then add ly, if string less than 3 leave unchanged """

    if len(my_inputs) >= VERBING_THRESHOLD:
        if my_inputs[-3:] == "ing":
            my_inputs = my_inputs + "ly"
        else:
            my_inputs = my_inputs + "ing"

        return my_inputs
    else:
        return my_inputs


# F. not_bad
# Given a string, find the first appearance of the
# substring 'not' and 'bad'. If the 'bad' follows
# the 'not', replace the whole 'not'...'bad' substring
# with 'good'.
# Return the resulting string.
# So 'This dinner is not that bad!' yields:
# This dinner is good!
def not_bad(my_input):
    """  find substrings not and bad, if bad follows not, replace not bad
    with good , if bad follows replace not...bad"""

    bad_var = 0
    not_var = 0

    word_list = my_input.split()

    for index, word in enumerate(word_list):
        if word == 'not':
            not_var = index
        if word.replace("!",'') == "bad":
            bad_var = index

    punctuation = ""
    if my_input[PUNCTUATION_CONST] in ".!;,":
        punctuation = my_input[PUNCTUATION_CONST]

    new_list = []
    if bad_var > not_var:
        for index, word in enumerate(word_list):
            if index >= not_var and index <= bad_var:
                new_list.append("good")
                break
            else:
                new_list.append(word)

        return " ".join(new_list) + punctuation

    else:
        return my_input


# G. front_back
# Consider dividing a string into two halves.
# If the length is even, the front and back halves are the same length.
# If the length is odd, we'll say that the extra char goes in the front half.
# e.g. 'abcde', the front half is 'abc', the back half 'de'.
# Given 2 strings, input1 and input2, return a string of the form
#  input1-front + input2-front + input1-back + input2-back
def front_back(input1, input2):
    """ merge front of input 1 + w/ back of input 2 w/ input 2 with back of
    input 1"""

    half1 = int(round(float(len(input1))/2.))
    half2 = int(round(float(len(input2))/2.))
    return input1[0:half1] + input2[0:half2] + input1[half1:] + input2[half2:]


def test(got, expected):
    """ simple test() function used in main() to print
        what each function returns vs. what it's supposed to return. """

    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
    """ main() calls the above functions with interesting inputs,
        using test() to check if each result is correct or not. """

    print '\nverbing'
    test(verbing('hail'), 'hailing')
    test(verbing('swiming'), 'swimingly')
    test(verbing('do'), 'do')

    print '\nnot_bad'
    test(not_bad('This movie is not so bad'), 'This movie is good')
    test(not_bad('This dinner is not that bad!'), 'This dinner is good!')
    test(not_bad('This tea is not hot'), 'This tea is not hot')
    test(not_bad("It's bad yet not"), "It's bad yet not")

    print '\nfront_back'
    test(front_back('abcd', 'xy'), 'abxcdy')
    test(front_back('abcde', 'xyz'), 'abcxydez')
    test(front_back('Kitten', 'Donut'), 'KitDontenut')


if __name__ == '__main__':
    main()