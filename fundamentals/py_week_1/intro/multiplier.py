"""Stav Grossfeld - multiplier program takes a string and
returns every character multiplied """


def multiplier():

    """This function receives a string and returns it
    with every character mutiplied."""

    my_string = raw_input("enter a string: ")

    new_str = ''.join([letter * 2 for letter in my_string])

    return new_str


def main():

    """This function calls and prints the output of the multiplier function."""

    print multiplier()

if __name__ == '__main__':
    main()
