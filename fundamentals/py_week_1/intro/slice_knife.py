"""Stav Grossfeld - slicer program takes a constant
MY_STRING and returns variations of slices"""


MY_STRING = "We were more than just a slice."


def my_slicer():

    """Doc String: This function takes a
    string and returns variations of slices."""

    print '1.', MY_STRING[:6]
    print '2.', MY_STRING[4:-3]
    print '3.', MY_STRING[::2]
    print '4.', MY_STRING[3:-1:2]
    print '5.', MY_STRING[-2::-1]


def main():

    """Doc String: Calls my_slicer function"""

    my_slicer()

if __name__ == '__main__':
    main()
