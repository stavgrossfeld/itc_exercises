"""Stav Grossfeld - This function takes strings of words,
and capitalizes each one and adds a
 left buffer by a 2nd arguement"""

V0 = ("aaaa", 0)
V1 = ("aaaa", 4)
V2 = ("aaaa", 5)
V3 = ("hello mY naME is iynigo Montoya", 40)
V4 = ("000hello mY naME is iynigo Montoya", 40)


def encode_title(title, len_title):

    """This function takes strings of words,
    and capitalizes each one and adds a
     left buffer by a 2nd arguement"""

    add_buffer = len_title - len(title)

    new_title = str(0)*add_buffer

    return new_title+title.title()


def main():
    """calls the different encode_title variations"""
    encode_title(V0[0], V0[1])
    encode_title(V1[0], V1[1])
    encode_title(V2[0], V2[1])
    encode_title(V3[0], V3[1])
    encode_title(V4[0], V4[1])

if __name__ == '__main__':
    main()
