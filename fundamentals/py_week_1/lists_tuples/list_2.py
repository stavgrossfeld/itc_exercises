"""Stav Grossfeld - series of exercises regardling lists"""


def remove_adjacent(nums):
    """removes multiple occurences of adjacent items in a list to only have
    one"""

    i = 1

    while i < len(nums):
        if nums[i] == nums[i - 1]:
            nums.pop(i)
            i -= 1
        i += 1
    return nums
# E. Given two lists sorted in increasing order, create and return a merged
# list of all the elements in sorted order. You may modify the passed in lists.
# Ideally, the solution should work in "linear" time, making a single
# pass of both lists.
#
# NOTE - DO NOT use return sorted(sorted1 + sorted2) - that's too easy :-)
#


def linear_merge(sorted1, sorted2):
    """returns linear merge for 2 sorted lists """
    sorted_list = []

    while sorted1 and sorted2:
        if sorted1[0] <= sorted2[0]:
            x = sorted1.pop(0)
        else:
            x = sorted2.pop(0)
        sorted_list.append(x)
    return sorted_list+sorted1+sorted2


def test(got, expected):
    """ simple test() function used in main() to print
        what each function returns vs. what it's supposed to return. """
    if got == expected:
        prefix = ' OK '
    else:
        prefix = '  X '
    print '%s got: %s expected: %s' % (prefix, repr(got), repr(expected))


def main():
    """ main() calls the above functions with interesting inputs,
        using test() to check if each result is correct or not. """

    print '\nremove_adjacent'
    test(remove_adjacent([1, 2, 2, 3]), [1, 2, 3])
    test(remove_adjacent([2, 2, 3, 3, 3, 2]), [2, 3, 2])
    test(remove_adjacent([]), [])

    print '\nlinear_merge'
    test(linear_merge(['aa', 'xx', 'zz'], ['bb', 'cc']),
         ['aa', 'bb', 'cc', 'xx', 'zz'])
    test(linear_merge(['aa', 'xx'], ['bb', 'cc', 'zz']),
         ['aa', 'bb', 'cc', 'xx', 'zz'])
    test(linear_merge(['aa', 'aa'], ['aa', 'bb', 'bb']),
         ['aa', 'aa', 'aa', 'bb', 'bb'])


if __name__ == '__main__':
    main()
