"""Stav Grossfeld - The program receives two elements each of them
is either a list or a tuple.It returns a list of tuples,
where the i-th tuple contains the i-th element from each
of the argument sequences or iterables."""

V1 = ([1, "hello world"], [4, 5, 6, 7, 8, 9])
V2 = ([1, 2, 3], [4, 5, 6])


def zipper(a, b):
    """The function receives two elements each of them
    is either a list or a tuple.It returns a list of tuples,
    where the i-th tuple contains the i-th element from each
    of the argument sequences or iterables."""

    list_length = min(len(a), (len(b)))

    new_list = []

    for i in range(0, list_length):
        new_list.append((a[i], b[i]))
    print new_list


def main():
    """Calls zipper function"""

    zipper(V1[0], V1[1])
    zipper(V2[0], V2[1])

if __name__ == '__main__':
    main()
