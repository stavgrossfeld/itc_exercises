""" Stav Grossfeld - same_same' that sums the members of a list,
 but can work with different data types,
all items in list are same type."""

MY_LIST_1 = ['a', 'b', 'c']
MY_LIST_2 = [2, 4, 5]
MY_LIST_3 = [[1, 2], [3, 4]]


def same_same(my_list):

    """This function sums the members of a list,
    but can work with different types as well,
    all items in list are same type."""

    new_list = my_list[0]
    for i in my_list[1:]:
        new_list = new_list + i
    return new_list


def main():

    """This function tests the different data structures,
    and calls same_same function"""

    same_same(MY_LIST_1)
    same_same(MY_LIST_2)
    same_same(MY_LIST_3)


if __name__ == '__main__':
    main()
