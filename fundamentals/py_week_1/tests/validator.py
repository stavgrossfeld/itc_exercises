"""Stav Grossfeld - prints product of numbers and validates the user_input
first"""

V1 = "5 5 5"
V2 = "-5 5 5"
V3 = "[5] 5 5"


def multiplier(user_input):
    """This function asks the user to provide a list of numbers delimited by
    spaces. Then, prints the product of all numbers."""

    product = 1

    for number in user_input.split():
        product = product * int(number)
    return int(product)


def validator(user_input):
    """This function validates the input"""

    digits = user_input.replace(' ', '').replace('-', '')

    if digits.strip().isdigit():
        return True
    else:
        return False

    return True


def main():
    """ Reads input from user and cleans, calls validator function and
    asserts the ouptut"""

    assert validator(V1)
    assert multiplier(V1)
    assert validator(V2)
    assert multiplier(V2)

    try:
        assert validator(V3)
    except:
        pass

    user_input = raw_input("enter numbers delimited by space: ").lstrip(

    ).rstrip()
    if validator(user_input) == False:
        main()
    else:
        print multiplier(user_input)

    return True


if __name__ == '__main__':
    a = main()
    assert a
