""" Stav Grossfeld - lambda dance exercise"""

def main():
    """ lambda practice returns several numerical manipulations"""
    a = 46
    b = 3

    divmod = lambda  x,y : (int(x)/y, x%y)

    non_negative = lambda x: 0 if x < 0 else x

    double_digits = lambda x: int(reduce(lambda x, y: x + y, map(lambda x: x
                                                                          * 2,
                                                            str(x))))

    assert divmod(4, 3) == (1, 1)
    assert divmod(4, 3) != (2,2)

    assert non_negative(-5) == 0
    assert non_negative(5) != 0

    assert double_digits(45) == 4455
    assert double_digits(45) != 5555

    print divmod(a,b)
    print non_negative(a)
    print double_digits(a)


if __name__ == "__main__":
    main()