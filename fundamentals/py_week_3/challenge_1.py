#challenge_1.py
import requests
import string
sub_dict = {"k":"m", "o":"q","e":"g"}
#sentence = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
sentence = "map"


#print sentence


import string

ALPHABET = string.lowercase


def substitution(key):
    key = (key % len(ALPHABET)) - 1

    key_dict = ALPHABET[key::1] + ALPHABET[0:key]

    decrypt_dict = {k: v for k, v in zip(key_dict, ALPHABET)}

    decrypt_dict_upper = {k: v for k, v in zip(key_dict.upper(),
                                               ALPHABET.upper())}

    decrypt_dict.update(decrypt_dict_upper)

    return decrypt_dict


def caesar_decrypt(encrypt, decrypt_dict):
    """accepts encryption, and decrypt dictionary, returns decrypted message"""
    return "".join([decrypt_dict[letter] if letter.lower() in
                            ALPHABET else letter for letter in encrypt])


def CBFA(message):

    for i in range(26):
        decrypt_dict = substitution(i)

        decrypt = caesar_decrypt(message, decrypt_dict)

        print decrypt, "\n\n"


def main():
    """ask for message, call encrypt function, call decrypt function"""

    message = sentence

    CBFA(message)


if __name__ == "__main__":
    main()
