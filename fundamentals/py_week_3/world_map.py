""" Stav Grossfeld - world map char manipulations"""

def world_map(countries):
    """takes countries and formats them to have uppercase for the first
    letter and lowercsae for other letters"""

    return map(lambda val: val.lower().title(), countries)



def main():
    """ lambda make countires uppercase letter"""
    countries = ["ISRAEL", "france", "engLand"]

    assert world_map(countries) == ["Israel", "France", "England"]

    print world_map(countries)


if __name__ == "__main__":
    main()