"""Stav Grossfeld - google log puzzle, download images and sort to
solve puzzle"""

import os
import re
import sys
import urllib
import pandas as pd


def read_urls(filename):
    """Returns a list of the puzzle urls from the given log file,
    extracting the hostname from the filename itself.
    Screens out duplicate urls and returns the urls sorted into
    increasing order."""

    with open(filename) as input_file:
        input_file = input_file.read()

    url_df = pd.DataFrame(input_file.split('\n'), columns=["logs"])

    puzzle_logs = url_df[url_df.logs.apply(lambda val: "puzzle" in str(val))]

    url_links = []
    for line in puzzle_logs.logs:
        match = re.search(r'"GET (\S+)', line)
        path = match.group(1)
        url = str(path)
        url_links.append(url)

    return pd.Series(sorted(set(url_links), key=url_sort_key))


def url_sort_key(url):
    """Used to order the urls in increasing order by 2nd word if present."""
    match = re.search(r'-(\w+)-(\w+)\.\w+', url)
    if match:
        return match.group(2)
    else:
        return url


def download_images(img_urls, dest_dir):
    """Given the urls already in the correct order, downloads
    each image into the given directory.
    Gives the images local filenames img0, img1, and so on.
    Creates an index.html in the directory
    with an img tag to show each local image file.
    Creates the directory if necessary.
    """

    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)

    index = file(os.path.join(dest_dir, 'index.html'), 'w')
    index.write('<html><body>\n')

    for num, url in enumerate(img_urls):

        url = "http://code.google.com"+url
        #print url

        local_name = dest_dir+"/"+"img"+str(num)+".jpg"
        urllib.urlretrieve(url, local_name)

        index.write('<img src="%s">' % (local_name,))


def main():
    """call funtions and accept system arguements"""

    args = sys.argv[1:]

    if not args:
        print 'usage: [--todir dir] logfile '
        sys.exit(1)

    todir = args[0][2:]

    if os.path.isfile(args[1]):
        img_urls = args[1]

    url_list = read_urls(img_urls)

    download_images(url_list, todir)


if __name__ == '__main__':
    main()
