"""Stav Grossfeld - accept list of numbers and return if all numbers are
divisable"""


def dividable(my_list, divisor):
    """use list comprehension to check input if divisable"""
    return all([num % divisor == 0 for num in my_list])


def main():
    """assert dividable function with test list, both outputs"""

    assert dividable([5,5,5,4], 5) == False

    assert dividable([5, 5, 5, 5], 5) == True

    dividable([5, 5, 5, 5], 5)

if __name__ == "__main__":
    main()
