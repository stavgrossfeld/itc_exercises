"""Stav Grossfeld - triple the letters in a string in one line"""


def triple(my_str):
    """use list comprehension to multiply string letters and join"""
    return "".join([letter*3 for letter in my_str])


def main():
    """assert triple function with test string"""

    assert triple("hello") == 'hhheeellllllooo'
    assert triple("") == ""

    triple("hello")


if __name__ == "__main__":
    main()
