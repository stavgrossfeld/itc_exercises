ENCRYPTED_MESSAGE = """YXu0hYq1xQ4,hQXUCw8Cs7nIu8BAxIUADYf\n\nLRYUYw5LI0RYUUYs6hDXU3UIoIBbcm\n.CBSA5LI7RYUUYs2hDXU8YBbnIj\n.UIoIBbnI2LI8RYUUYs7hDXU2cYBbnQZ\n.cYBbnQo1LI9RYUUYs7hDXU4UDBk\n.JYUDoIBbnQo4LI3RYUUYs8hDXU6YLRDbj\n.JYULYh8LI3RYUUYs8hDXU1CUIBIsDJDYH\n.YLhYJ7BDIoYbj\n.LUhAQo0LYLDo3U'hYRD2BDIoYbL2XSAQhY0QU0MDYRs0YXU4XSAQXUBW\n.LYBAR8CUIBDoIUoDRb1LUDYs2LRQRRm\n.CUIRAb3JBAQXL6RYlYh9LLDb0LLYBha\n.CBUhYBIL0CBUIoIBbcY9hg\n.JYohYBIL1YXU5YoDx7xQ6,CUIASIsnD8YLAxYR0YXU9hQIUDUbnYU4QU3YRYXu\n.LLYAS1JBAQXL7Ys3--YhQ2JhD2CBsDRYxYRb2CBhQ9YhQ6LAQIlsQ--9CDO7QU1QJ0XSAQXUBW\n.UI4UDXU5CDO0CDn2UQh7Ys2LAQIlsQ0UD2ULRIx8LLYBhA2YR'AQC1OQe\n.XoUAv7LI3RYUUYs0hDXU8XSAQXUBW\n.RYlYh0RYlYh5LI9hYUxQ0RYUUYs2hDXU8*UXSIR*9xg\n.OQh4YXU3hQIUDUhYnYBbnI9LI1JRDX0QU6,hIDBbcY5L'UI3D7JDs6xg\n.DYJI4YXU6hQIUDUhYnYBbnI9LI9CLDY4QU3,hIDBbcY2UI5CDn4Ys8D8JQQS1LYoDbLYnDe\n.DYJI0YRD9YhQ6ShIMhQX2UDYRS4DYJI3--9L'UYB5QJ8YRQn9xQ0!YLQXU"""

SUBSTITUION_DICTIONARY = {'s': 'L', 'b': 's', 'w': 'O', 'z': 'G', 'c': 'o',
                          'J': 'y', 'V': 't', 'P': 'w', 'B': 'f', 'Z': 'q',
                          'F': 'k',
                          'O': 'N', 'u': 'A', 'W': 'r', 'K': 'K', 'a': 'D',
                          'v': 'l',
                          'g': 'S', 'f': 'x', 'x': 'c', 'N': 'e', 'p': 'b',
                          'U': 'a',
                          'j': 'P', 'o': 'Q', 'i': 'I', 'M': 'd', 't': 'U',
                          'H': 'V',
                          'X': 'i', 'Y': 'T', 'R': 'H', 'h': 'X', 'L': 'z',
                          'G': 'F', 'A': 'W', 'm': 'n', 'T': 'u', 'l': 'B',
                          'C': 'Z',
                          'q': 'p', 'D': 'v', 'I': 'g', 'n': 'h', 'y': 'C',
                          'S': 'j',
                          'k': 'M', 'd': 'J', 'Q': 'E', 'e': 'Y', 'r': 'R',
                          'E': 'm'}


def reverse_dictionary(input_dict):
    """take dictionary and swap the key and vals"""

    return {v: k for k, v in input_dict.iteritems()}


def decrypted(encrypted,new_dict):
    """use new dictionary to decrypt the message"""

    return [new_dict[letter]
                if letter in new_dict.keys() else letter
                        for letter in encrypted]


def denumbered(decrypted_message):
    """replace each number with a space"""

    return "".join([" " if letter in "0123456789" else letter for
                    letter in
                    decrypted_message])


def reverse_each_word(denumbered_message):
    """ reverse each word"""

    return " ".join([word[::-1] for word in denumbered_message.split(' ')])


def main():
    """ calls the functions in order to decrypt the message"""

    assert reverse_dictionary({'a':'b'}) == {'b':'a'}

    assert decrypted('blah blah',{'b':'f','h':'u'}) == ['f', 'l', 'a', 'u',
     ' ' ,'f' ,'l' ,'a' ,'u']

    assert denumbered('bla1h') == 'bla h'

    assert reverse_each_word("hello world") == "olleh dlrow"


    reversed_dict = reverse_dictionary(SUBSTITUION_DICTIONARY)

    decrypted_message = decrypted(ENCRYPTED_MESSAGE, reversed_dict)

    denumbered_message = denumbered(decrypted_message)

    print reverse_each_word(denumbered_message)



if __name__ == '__main__':
    main()
