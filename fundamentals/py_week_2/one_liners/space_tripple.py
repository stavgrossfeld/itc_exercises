"""Stav Grossfeld - returns another string where each word in the
original string is tripled."""


def space_triple(my_str):
    """use list comprehension to multiply string words and join"""
    return " ".join([letter*3 for letter in my_str.split()])


def main():
    """assert space_tripple function with test string"""

    assert space_triple("hello") == 'hellohellohello'
    assert space_triple("") == ""
    assert space_triple("hello world") == "hellohellohello worldworldworld"

    space_triple("hello")
if __name__ == "__main__":
    main()
