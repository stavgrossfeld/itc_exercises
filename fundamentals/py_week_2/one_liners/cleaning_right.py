"""Stav Grossfeld - cleans strings w/ letters c o f or e if its on the
 right of the string"""


def clean_cofe(my_list):
    """use list comprehension to clean string words from cofe and join"""
    return [word.rstrip('cofe ') for word in my_list]

def main():
    """assert clean_cofe function with test list"""

    assert clean_cofe(["caaaeeefocfe", "hello cofe"]) == ['caaa', 'hell']

    clean_cofe(["caaaeeefocfe", "hello cofe"])

if __name__ == "__main__":
    main()
