"""Stav Grossfeld - command calculator, accept two numbers and a
mathematical operation as an input and print output"""

import argparse


def add(inputs):
    """add the input numbers"""
    return inputs[0] + inputs[1]


def multiply(inputs):
    """multiply the input numbers"""
    return inputs[0] * inputs[1]


def subtract(inputs):
    """subtract the input numbers"""
    return inputs[0] - inputs[1]


def divide(inputs):
    """divide the input numbers"""
    if inputs[1] == 0:
        print "cant divide by 0, bye"
        exit(0)
    else:
        return float(inputs[0]) / inputs[1]

def good_morning():
    print "good morning :)"


def main():
    """receive arguments from arg parser, and call the function that matches
    the user input function dep"""

    assert add((5,0)) == 5
    assert multiply((5,0)) == 0
    assert divide((6,2)) == 3


    parser = argparse.ArgumentParser(description='Process some integers.')

    parser.add_argument(dest="operation", nargs=1, type=str,
                        choices=["add", 'divide', 'subtract',
                                 'multiply'])
    parser.add_argument('input', type=float, nargs=2, help="enter two numbers")

    parser.add_argument("--good_morning", action="store_true",
                        required=False, help= "welcome")

    inputs = parser.parse_args().input

    operation = parser.parse_args().operation[0]



    if parser.parse_args().good_morning:
        good_morning()

    result = eval(operation + '(%s)' % inputs)

    print result
if __name__ == "__main__":
    main()
