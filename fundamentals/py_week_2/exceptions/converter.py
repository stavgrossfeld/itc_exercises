"""program receives path of excel file with one filled column where every
cell includes length in meters, new file contains original column and
converted into feet column"""

import csv
import os
import pandas as pd

CONVERSION = 3.28084


def converter(filepath):
    """try to open file, create """


    with open(filepath, "r") as input_file:
        my_file = input_file.read()

    try:
        meters = [float(meter) for meter in my_file.split()]
    except:
         ValueError("file does not consist of floats")
    converted = [CONVERSION * meter for meter in meters]

    file_path_split = filepath.split('.')

    new_filepath = file_path_split[0] + "_new" + "." + file_path_split[-1]

    print "this is your new file_path: ", new_filepath

    final = zip(meters, converted)

    with open(new_filepath, 'w') as csv_file:

        writer = csv.writer(csv_file, delimiter = ",")

        for i in range(0, len(final)):

            writer.writerow((final[i][0], final[i][1]))


def main():
    """ Calls converter function """
    file_path = raw_input("enter full file path: ")


    if not os.path.exists(file_path):
        print "file path does not exist"
        main()

    if not file_path.endswith('.csv'):
        print "not csv file"
        main()

    if pd.read_csv(file_path).shape[1] != 1:
        print "not one column"
        main()

    with open(file_path, "r") as input_file:
        my_file = input_file.read()
        try:
            meters = [float(meter) for meter in my_file.split()]
        except:
            print " you have non numeric input"
            main()

    converter(file_path)


if __name__ == "__main__":
    main()
