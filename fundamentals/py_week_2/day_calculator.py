"""Stav Grossfeld - day calculator receives # days as integer print date and
time in number days from now in iso"""

import datetime
from datetime import timedelta


def day_calc(num_days):
    """check valid input from user, if valid, print datetime + num_days,
    else call main()"""

    if num_days.isdigit():
        new_time = datetime.datetime.now() + timedelta(days=int(num_days))
        print new_time.isoformat()

        return True
    else:
        main()
        return True


def main():
    """request input from user, assert day_calc function"""

    num_days = raw_input("enter number of days from now for date output: ")
    assert day_calc(num_days)


if __name__ == '__main__':
    main()
