"""Stav Grossfeld - implement caesar algorithm of cryptography, 3 letters
forward"""

import string

ALPHABET = string.lowercase


def substitution(key):
    key = (key % len(ALPHABET)) - 1

    key_dict = ALPHABET[key::1] + ALPHABET[0:key]

    decrypt_dict = {k: v for k, v in zip(key_dict, ALPHABET)}

    decrypt_dict_upper = {k: v for k, v in zip(key_dict.upper(),
                                               ALPHABET.upper())}

    decrypt_dict.update(decrypt_dict_upper)

    return decrypt_dict


def caesar_decrypt(encrypt, decrypt_dict):
    """accepts encryption, and decrypt dictionary, returns decrypted message"""
    return "".join([decrypt_dict[letter] if letter.lower() in
                            ALPHABET else letter for letter in encrypt])


def CBFA(message):

    for i in range(26):
        decrypt_dict = substitution(i)

        decrypt = caesar_decrypt(message, decrypt_dict)

        print decrypt, "\n\n"


def main():
    """ask for message, call encrypt function, call decrypt function"""

    message = """Uowig Xizwig Qosgof kog o Fcaob ghohsgaob, usbsfoz obr bchopzs
     oihvcf ct Zohwb
dfcgs. Vs dzomsr o qfwhwqoz fczs wb hvs sjsbhg hvoh zsr hc hvs rsawgs ct hvs
 Fcaob
Fsdipzwq obr hvs fwgs ct hvs Fcaob Sadwfs. Wb 60 PQ, Qosgof, Qfoggig, obr
 Dcadsm
tcfasr o dczwhwqoz ozzwobqs hvoh kog hc rcawbohs Fcaob dczwhwqg tcf gsjsfoz
msofg. Hvswf ohhsadhg hc oaogg dcksf hvfciuv dcdizwgh hoqhwqg ksfs cddcgsr
pm
hvs qcbgsfjohwjs fizwbu qzogg kwhvwb hvs Fcaob Gsbohs, oacbu hvsa Qohc hvs
Mcibusf kwhv hvs tfseisbh giddcfh ct Qwqsfc. Qosgof'g jwqhcfwsg wb hvs Uozzwq
Kofg, qcadzshsr pm 51 PQ, slhsbrsr Fcas'g hsffwhcfm hc hvs Sbuzwgv Qvobbsz obr
hvs Fvwbs. Qosgof psqoas hvs twfgh Fcaob usbsfoz hc qfcgg pchv kvsb vs piwzh o
pfwrus oqfcgg hvs Fvwbs obr qcbriqhsr hvs twfgh wbjogwcb ct Pfwhowb."""

    CBFA(message)


if __name__ == "__main__":
    main()
