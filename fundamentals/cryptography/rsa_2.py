RSA_PUBLIC_KEY = (3233, 17) # n = 253, e = 3
RSA_PRIVATE_KEY = 2753

def factor_pub_key(n): # factor public key to p and q

	for factor in range(2,n):
		if n % factor == 0:
			p = factor
			q = n / factor
	return p, q

def generate_private(p,q):
	phi_n = (p-1) * (q-n)



def encrypt_message(x, e, n):
	cipher = x**e % n
	return cipher

def decrypt_message(cipher, d, n):
	deciphered = cipher**d % n
	return deciphered 

def main():
	x = 42 # my message
	n = RSA_PUBLIC_KEY[0]
	e = RSA_PUBLIC_KEY[1]
	d = RSA_PRIVATE_KEY

	cipher = encrypt_message(x, e, n)
	decrypt_cipher = decrypt_message(cipher, d, n)


	print "og message: ", x
	print "cipher: ", cipher 
	print "decrypted cipher:", decrypt_cipher


	p, q = factor_pub_key(n)

	#print p, q



if __name__ == "__main__":
	main()