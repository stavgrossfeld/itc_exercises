"""Stav Grossfeld - find prime factors for following numbers"""
A = 15
B = 391
C = 13231
D = 34637683 ####################
E = 112725371358746879
F = 121932631356500565915104429680536767
G = 1629628781067588869014756550727499815854740557492702399102538248404844868996
6694114836419503

import itertools 
import math

def factorization(num):
    """factor numbers""" 

    factors = []


     # only need to go up to square root + 1
    
    max_val = int(math.ceil(num**.5))
    min_val = max_val-100000
    #print num, " sqrt:",max_val
  
    factor = 2

    if num % 2 == 0:
        step = 2
    else:
        step = 1

    while factor < max_val:
        if num % factor == 0:
            p = factor
            q = num / p
            factors = [p,q]
            break
        else:
            factor = factor + step
    
 
    print num, "factors: ", factors

    # except:
    #     step = 1
    #     max_val = num**2 + 1 


    #     while len(factors) < 2:
    #         for factor in itertools.count(1,step):

    #             if (max_val / float(factor)) % 1 == 0:
    #                 factors.append(factor)


def main():
    """ call factorization of different inputs"""

    factorization(A)
    factorization(B)
    factorization(C)
    factorization(D)
    factorization(E)
    factorization(F)
    #factorization(G)

if __name__ == "__main__":
    main()