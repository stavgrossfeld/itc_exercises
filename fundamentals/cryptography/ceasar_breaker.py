"""Stav Grossfeld - implement caesar algorithm of cryptography, all variations and select best plain text score"""

import string
from PyDictionary import PyDictionary
import operator
from nltk.corpus import words

ALPHABET = string.lowercase
dictionary = PyDictionary

ENGLISH_FREQUENCY_LETTER = {'a': 8.17, 'c': 2.78, 'b': 1.29, 'e': 12.7, 'd': 4.25, 'g': 2.02, 'f': 2.23, 'i': 6.97, 'h': 6.09, 'k': 0.77,
                            'j': 0.15, 'm': 2.41, 'l': 4.03, 'o': 7.51, 'n': 6.75, 'q': 0.1, 'p': 1.93, 's': 6.33, 'r': 5.99, 'u': 2.76,
                            't': 9.06, 'w': 2.36, 'v': 0.98, 'y': 1.97, 'x': 0.15, 'z': 0.07}


def substitution(key):
    """ create a dictionary for a ceasar cypher with a key"""
    key = (key % len(ALPHABET)) - 1

    key_dict = ALPHABET[key::1] + ALPHABET[0:key]

    decrypt_dict = {k: v for k, v in zip(key_dict, ALPHABET)}

    decrypt_dict_upper = {k: v for k, v in zip(key_dict.upper(),
                                               ALPHABET.upper())}

    decrypt_dict.update(decrypt_dict_upper)

    return decrypt_dict


def caesar_decrypt(encrypt, decrypt_dict):
    """accepts encryption, and decrypt dictionary, returns decrypted message"""
    return "".join([decrypt_dict[letter] if letter.lower() in
                            ALPHABET else letter for letter in encrypt])


def plain_text_score(string):
    """Score a string according the the most common letter"""
    return sum([ENGLISH_FREQUENCY_LETTER[char.lower()]**3 for char in string if char in ENGLISH_FREQUENCY_LETTER])


def CBFA(message):
    """creates dictionary of messages and selects their max score"""
    dictionary = PyDictionary()

    decrypt_list = []
    for char in range(26):

        decrypt_dict = substitution(char)

        decrypt = caesar_decrypt(message, decrypt_dict)

        decrypt_list.append(decrypt)

    decrypted_score_dict = {}

    for decrypted_message in decrypt_list:
        decrypted_score_dict[decrypted_message] = plain_text_score(decrypted_message)

    print max(zip(decrypted_score_dict.values(), decrypted_score_dict.keys()))[1]


def main(): 
    """ask for message, call encrypt function, call main CBFA function"""

    message = """Uowig Xizwig Qosgof kog o Fcaob ghohsgaob, usbsfoz obr bchopzs
     oihvcf ct Zohwb
dfcgs. Vs dzomsr o qfwhwqoz fczs wb hvs sjsbhg hvoh zsr hc hvs rsawgs ct hvs
 Fcaob
Fsdipzwq obr hvs fwgs ct hvs Fcaob Sadwfs. Wb 60 PQ, Qosgof, Qfoggig, obr
 Dcadsm
tcfasr o dczwhwqoz ozzwobqs hvoh kog hc rcawbohs Fcaob dczwhwqg tcf gsjsfoz
msofg. Hvswf ohhsadhg hc oaogg dcksf hvfciuv dcdizwgh hoqhwqg ksfs cddcgsr
pm
hvs qcbgsfjohwjs fizwbu qzogg kwhvwb hvs Fcaob Gsbohs, oacbu hvsa Qohc hvs
Mcibusf kwhv hvs tfseisbh giddcfh ct Qwqsfc. Qosgof'g jwqhcfwsg wb hvs Uozzwq
Kofg, qcadzshsr pm 51 PQ, slhsbrsr Fcas'g hsffwhcfm hc hvs Sbuzwgv Qvobbsz obr
hvs Fvwbs. Qosgof psqoas hvs twfgh Fcaob usbsfoz hc qfcgg pchv kvsb vs piwzh o
pfwrus oqfcgg hvs Fvwbs obr qcbriqhsr hvs twfgh wbjogwcb ct Pfwhowb."""

    CBFA(message)


if __name__ == "__main__":
    main()
