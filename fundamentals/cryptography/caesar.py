"""Stav Grossfeld - implement caesar algorithm of cryptography, 3 letters
forward"""

import string

ALPHABET = string.lowercase


def substitution(key):
    key = (key % len(ALPHABET)) - 1

    key_dict = ALPHABET[key::1] + ALPHABET[0:key]

    encrypt_dict = {k: v for k, v in zip(ALPHABET, key_dict)}
    encrypt_dict_upper = {k: v for k, v in zip(ALPHABET.upper(),
                                               key_dict.upper())}

    encrypt_dict.update(encrypt_dict_upper)

    decrypt_dict = {k: v for k, v in zip(key_dict, ALPHABET)}

    decrypt_dict_upper = {k: v for k, v in zip(key_dict.upper(),
                                               ALPHABET.upper())}

    encrypt_dict.update(encrypt_dict_upper)
    decrypt_dict.update(decrypt_dict_upper)

    return encrypt_dict, decrypt_dict


def caesar_encrypt(message, encrypt_dict):
    """accepts message, and encrypt dictionary, returns encrypted message"""

    return "".join([encrypt_dict[letter] if letter.lower() in
                        ALPHABET else letter for letter in message])


def caesar_decrypt(encrypt, decrypt_dict):
    """accepts encryption, and decrypt dictionary, returns decrypted message"""
    return "".join([decrypt_dict[letter] if letter.lower() in
                            ALPHABET else letter for letter in encrypt])


def main():
    """ask for message, call encrypt function, call decrypt function"""

    message = raw_input("enter message to encrypt and decrypt: ")
    key = input("enter an integer for a key: ")

    encrypt_dict, decrypt_dict = substitution(3)

    encrypt = caesar_encrypt(message, encrypt_dict)

    decrypt = caesar_decrypt(encrypt, decrypt_dict)

    print encrypt
    print decrypt


if __name__ == "__main__":
    main()
