

import string

ALPHABET = string.lowercase


def substitution(key):
    key = (key % len(ALPHABET)) - 1

    key_dict = ALPHABET[key::1] + ALPHABET[0:key]

    encrypt_dict = {k: v for k, v in zip(ALPHABET, key_dict)}
    encrypt_dict_upper = {k: v for k, v in zip(ALPHABET.upper(),
                                               key_dict.upper())}

    encrypt_dict.update(encrypt_dict_upper)

    decrypt_dict = {k: v for k, v in zip(key_dict, ALPHABET)}

    decrypt_dict_upper = {k: v for k, v in zip(key_dict.upper(),
                                               ALPHABET.upper())}

    encrypt_dict.update(encrypt_dict_upper)
    decrypt_dict.update(decrypt_dict_upper)

    return encrypt_dict, decrypt_dict


def caesar_encrypt(message, encrypt_dict):
    """accepts message, and encrypt dictionary, returns encrypted message"""

    return "".join([encrypt_dict[letter] if letter.lower() in
                        ALPHABET else letter for letter in message])


def caesar_decrypt(encrypt, decrypt_dict):
    """accepts encryption, and decrypt dictionary, returns decrypted message"""
    return "".join([decrypt_dict[letter] if letter.lower() in
                            ALPHABET else letter for letter in encrypt])


def RSAGeneratePrivateKey(p,q,e):
    """Generate private key looping over numbers from 1,N (p*q) and check
    result using
    eulers theorem and return private keys"""

    N = p*q
    phi_N = (p-1)*(q-1)

    for priv_keys in range(N):

        if (priv_keys * e) % (phi_N) == 1:
            d = priv_keys
        else:
            continue

    return N, phi_N, d

def RSAenc(x, N, e):
    """Encrypt message using public key"""
    encrypted = x**e % N
    return encrypted


def RSAdec(x, N, d):  # x = message N = pq e=3
    """ decrypt message using private key"""
    decrypted = (x ** d) % N
    return decrypted




def main():
	"""generate rsa keys, ask for message, call encrypt function with private rsa key, call decrypt function"""

	x = 25  # message

    p = 5
    q = 11
    e = 3

    N, phi_N, d = RSAGeneratePrivateKey(p,q,e)

    encrypted =  RSAenc(x, N, e)

    print "original message bits: ", x

    print "encrytped message: ", encrypted

    decrypted = RSAdec(encrypted, N, d)

    print "decrypted: ", decrypted



    # message = raw_input("enter message to encrypt and decrypt: ")
    # key = x


    # encrypt_dict, decrypt_dict = substitution(3)

    # encrypt = caesar_encrypt(message, encrypt_dict)

    # decrypt = caesar_decrypt(encrypt, decrypt_dict)

    # print encrypt
    # print decrypt

if __name__ == "__main__":
    main()