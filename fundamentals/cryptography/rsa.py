"""Stav Grossfeld - Implement RSAEnc(x, N, e) and RSADec(x, N, d).
Next implement RSAGeneratePrivateKey, which receives p,q,e and
calculates N,d

N = pq
X = message
e = 3 the choice number

"""

def RSAGeneratePrivateKey(p,q,e):
    """Generate private key looping over numbers from 1,N (p*q) and check
    result using
    eulers theorem and return private keys"""

    N = p*q
    phi_N = (p-1)*(q-1)

    for priv_keys in range(phi_N):

        if (priv_keys * e) % (phi_N) == 1:
            d = priv_keys
        else:
            continue

    return N, phi_N, d

def RSAenc(x, N, e):
    """Encrypt message using public key"""
    encrypted = x**e % N
    return encrypted
 

def RSAdec(x, N, d):  # x = message N = pq e=3
    """ decrypt message using private key"""
    decrypted = (x ** d) % N
    return decrypted

def main():
    """Generate private key, encrypt message, decrypt message"""

    x = 25

    p = 5
    q = 11
    e = 3

    N, phi_N, d = RSAGeneratePrivateKey(p,q,e)

    encrypted =  RSAenc(x, N, e)

    print "original message bits: ", x

    print "encrytped message: ", encrypted

    decrypted = RSAdec(encrypted, N, d)

    print "decrypted: ", decrypted


if __name__ == "__main__":
    main()
