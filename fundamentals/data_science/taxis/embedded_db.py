#-*- coding: utf-8 -*-

import sqlite3
import zipfile
import codecs
import csv
import datetime


"""1"""
zfile = zipfile.ZipFile("./train.csv.zip")
with zfile.open('train.csv', 'rU') as readFile:
	count = 0
	for line in codecs.iterdecode(readFile, "utf8"):
		if count < 2:
			print (line)
			count = count + 1
		else:
			break

"""2"""

count = 0
with zfile.open("train.csv", "r") as readFile:
    decoded = codecs.iterdecode(readFile, "utf8")
    for row in csv.DictReader(decoded,delimiter = ","):
        t = datetime.date.fromtimestamp(float(row['TIMESTAMP']))
        t = t.strftime('%Y-%m-%d')
        if t[5:] == "10-14":
            count = count + 1
        else:
            continue

# 4982

# 15 second intervals for json

"""3"""
count = 0
length_trip_total = 0
with zfile.open("train.csv", "r") as readFile:
	decoded = codecs.iterdecode(readFile, "utf8")
	for row in csv.DictReader(decoded,delimiter = ","):
		t = datetime.date.fromtimestamp(float(row['TIMESTAMP']))
		t = t.strftime('%Y-%m-%d %H:%M:%S')



		if (t[5:10] == "10-14") and (t[11:] <= "13:00:00"):

			length_trip = len(row["POLYLINE"])

			# get one minute of


			count = count + 1
			length_trip_total = length_trip_total + length_trip # 15s each trip

		else:
			continue
print ("total trips on my bday: ",count)
print ("length of trips on my bday: ",(length_trip_total * 15) / 3600)




"""4
$ load sqllite database
1.create table w/ columsn in readFile2. insert and commit 10k trips at a 
datetime3 using database find number of trips per day before and after 1 as 
well as their total drive time"""

import sqlite3

with sqlite3.connect("db") as con:
	cur = con.cursor()

	print cur
	cur.execute("""DROP TABLE taxis;""")
	cur.execute("""CREATE TABLE taxis (
	TRIP_ID bigint,
	CALL_TYPE str,
	ORIGIN_CALL str,
	ORIGIN_STAND str,
	TAXI_ID bigint,
	TIMESTAMP str,
	DAYTYPE str,
	MISSING_DATA boolean,
	POLYLINE str)
	""")
	zfile = zipfile.ZipFile("./train.csv.zip")
	with zfile.open("train.csv", 'r') as readFile:
		decoded = codecs.iterdecode(readFile, "utf8")
		for index, row in enumerate(csv.DictReader(decoded, delimiter = ",")):
			#insert a trip
			#print row.values()
			cur.execute("INSERT INTO taxis VALUES (?, ?, ?,?, ? , ?,?, ?, ?)", tuple(row.values()))
			if index % 10000 == 0:
			# commit every 10k
				con.commit()
				print (index)
		#commit remainder
		con.commit()
	cur.close()

"""5"""
print(5)
with sqlite3.connect("db") as con:
	cur = con.cursor()
	cur.execute("pragma table_info(taxis)")
	print cur.fetchall()

	cur.execute("SELECT TIME(TIMESTAMP) FROM taxis LIMIT 100")
	print cur.fetchall()


"""Using the database find the number of trips per day before and after 13:00 as well as
#their total trip time"""

"""
SELECT 

COUNT(*),
CASE WHEN  time >= "13:00:00" THEN "after_1" ELSE "before_1" END AS threshold
	FROM(
	select 
	  time(TIMESTAMP, 'unixepoch') AS time
	FROM taxis)
GROUP BY 
	threshold

821928|after_1
888742|before_1
"""

