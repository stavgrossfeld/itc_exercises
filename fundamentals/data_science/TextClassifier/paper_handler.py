import re

import mysql.connector
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
import numpy as np


stopwords = set(stopwords.words('english'))
stemmer = PorterStemmer()

splitter = re.compile('\\W*')
def getwords(doc):
    # Split the words by non-alpha characters
    words = [s.lower() for s in splitter.split(doc)
             if (
                 (len(s) > 2) and
                 (len(s) < 20) and
                 (s not in stopwords)
             )
             ]
    words = [stemmer.stem(w) for w in words]
    # Return the unique set of words only
    return dict([(w, 1) for w in words])


def execute_sql(cnx, sql):
    cursor = cnx.cursor()
    lines = [l.strip() for l in sql.splitlines() if l.strip()]
    for l in lines:
        if not l.endswith(';'):
            l += ';'
        cursor.execute(l)
    try:
        cnx.commit()
    except:
        pass
    res = [r for r in cursor]
    cursor.close()
    return res


class DocHandler:
    def __init__(self, getfeatures, reset_db=False, **kwargs):
        user = kwargs.get('user', 'root')
        password = kwargs.get('password', '')
        self.database = 'python_papers'
        if reset_db:
            self.con = mysql.connector.connect(user=user, password=password,
                                           host='127.0.0.1')
            self.setdb()
        self.con = mysql.connector.connect(user=user, password=password,
                                           host='127.0.0.1',
                                           db=self.database
                                           )
        # Counts of feature/category combinations
        self.feature_category = {}
        # Counts of documents in each category
        self.counts_category = {}
        self.getfeatures = getfeatures

    def _execute_sql(self, sql):
        return execute_sql(self.con, sql)

    def setdb(self):
        self._execute_sql("""
        DROP SCHEMA IF EXISTS %s;
        CREATE SCHEMA %s;
        USE %s;
        CREATE TABLE IF NOT EXISTS feature_category(feature TEXT, category TEXT, count INT);
        CREATE TABLE IF NOT EXISTS author_category(author TEXT, category TEXT, count INT);
        CREATE TABLE IF NOT EXISTS counts_category(category TEXT, count INT);
        ALTER TABLE feature_category CONVERT TO CHARACTER SET utf8;
        ALTER TABLE author_category CONVERT TO CHARACTER SET utf8;
        ALTER TABLE counts_category CONVERT TO CHARACTER SET utf8;
        """ % (self.database, self.database, self.database)
        )


    def inc_feature(self, f, cat):
        count = self.fcount(f, cat)
        if count == 0:
            self._execute_sql("INSERT INTO feature_category VALUES ('%s','%s',1)"
                              % (f, cat))
        else:
            self._execute_sql(
                "UPDATE feature_category SET count=%d WHERE feature='%s' AND category='%s'"
                % (count + 1, f, cat))

    def inc_author(self, a, cat):
        count = self.fcount(a, cat)
        if count == 0:
            self._execute_sql("INSERT INTO author_category VALUES ('%s','%s',1)"
                              % (a, cat))
        else:
            self._execute_sql(
                "UPDATE author_category SET count=%d WHERE feature='%s' AND category='%s'"
                % (count + 1, a, cat))

    def fcount(self, f, cat):
        res = self._execute_sql(
            'SELECT count FROM feature_category WHERE feature="%s" AND category="%s"'% (f, cat))
        if res == []:
            return 0
        else:
            return float(res[0][0])

    def incc(self, cat):
        count = self.catcount(cat)
        if count == 0:
            self._execute_sql("INSERT INTO counts_category VALUES ('%s',1)" % (cat))
        else:
            self._execute_sql("UPDATE counts_category SET count=%d WHERE category='%s'"
                              % (count + 1, cat))

    def catcount(self, cat):
        res = self._execute_sql('SELECT count FROM counts_category WHERE category="%s"'
                                % (cat))
        if res == []:
            return 0
        else:
            return float(res[0][0])

    def categories(self):
        cur = self._execute_sql('SELECT category FROM counts_category');
        return [d[0] for d in cur]

    def total_count(self):
        res = self._execute_sql('SELECT SUM(count) FROM counts_category')
        if res == []:
            return 0
        else:
            return float(res[0][0])

    def text_from_paper(self, paper):
        text = paper['title'] #  + ' ' + paper['abstract']
        return text

    def train(self, paper, cat):
        text = self.text_from_paper(paper)
        features = self.getfeatures(text)
        # Increment the count for every feature with this category
        for f in features:
            self.inc_feature(f, cat)

        for a in paper['authors']:
            tokens = a.split(',')
            tokens = [re.sub(r'\W', '', t.strip()) for t in tokens]
            a = ', '.join(tokens)
            self.inc_author(a, cat)

        # Increment the count for this category
        self.incc(cat)


class NaiveBayes(DocHandler):
    def __init__(self, getfeatures, smoothing_alpha=1.0, **kwargs):
        DocHandler.__init__(self, getfeatures, **kwargs)
        self.smoothing_alpha = smoothing_alpha
        self.thresholds = {}

    def docprobscore(self, item, cat):
        features = self.getfeatures(item)
        probabilities = [self.category_given_feature_prob(f, cat) for f in features]
        return np.sum(np.log(probabilities))


    def probscore(self, item, cat):
        catprobscore = np.log(self.catcount(cat) / self.total_count())
        docprobscore = self.docprobscore(item, cat)
        return docprobscore + catprobscore

    def setthreshold(self, cat, t):
        self.thresholds[cat] = t

    def getthreshold(self, cat):
        if cat not in self.thresholds: return 1.0
        return self.thresholds[cat]

    def feature_given_category_prob(self, f, cat):
        if self.catcount(cat) == 0: return 0
        # The total number of times this feature appeared in this
        # category divided by the total number of items in this category
        return (self.fcount(f, cat) + self.smoothing_alpha) / (self.catcount(cat) + self.smoothing_alpha)

    def category_probability(self, cat):
        category_count = self.catcount(cat)
        if category_count == 0:
            return 0
        else:
            return (category_count + self.smoothing_alpha) / (self.total_count() + self.smoothing_alpha)

    def category_given_feature_prob(self, f, cat):
        conditional_prob = self.feature_given_category_prob(f, cat)
        prior_prob = self.category_probability(cat)
        return prior_prob * conditional_prob

    def classify(self, paper, default=None):
        text = self.text_from_paper(paper)
        probscores = {}
        # Find the category with the highest probability
        max_ = -np.inf
        best = default
        min_ = np.inf
        for cat in self.categories():
            probscores[cat] = self.probscore(text, cat)
            if probscores[cat] > max_:
                max_ = probscores[cat]
                best = cat
            if probscores[cat] < min_:
                min_ = probscores[cat]
        return best, max_ - min_




if __name__ == '__main__':
    import json
    import random
    classifier = NaiveBayes(getwords, reset_db=True, password='')
    papers = json.load(open('tagged_papers.json'))
    random.shuffle(papers)
    for p in papers:
        title = p['title']
        abstract = p['abstract']
        txt = "%s\n%s\n\n" %(title,abstract)
        tag = p.get('tag', '???')
        prediction, prediction_score = classifier.classify(p)
        classifier.train(p, p['tag'])
        result = classifier.classify(p)
        pass
