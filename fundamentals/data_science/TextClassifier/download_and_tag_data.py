import collections
import json
import random

from Bio import Entrez


def flatten(d, parent_key='', sep='_'):
    ''' Flatten a nested dict '''
    # https://stackoverflow.com/a/6027615/17523
    items = []
    for k, v in d.items():
        new_key = parent_key + sep + k if parent_key else k
        if isinstance(v, collections.MutableMapping):
            items.extend(flatten(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def search(query, retmax=200):
    # Based on
    # https://marcobonzanini.com/2015/01/12/searching-pubmed-with-python/
    Entrez.email = 'xyz@abc.net'
    handle = Entrez.esearch(db='pubmed',
                            sort='relevance',
                            retmax=str(retmax),
                            retmode='xml',
                            term=query)
    results = Entrez.read(handle)
    id_list = results['IdList']
    papers = fetch_details(id_list)
    papers = [parse_paper(p) for p in papers]
    papers = [p for p in papers if p is not None]
    return papers


def fetch_details(id_list):
    # Based on
    # https://marcobonzanini.com/2015/01/12/searching-pubmed-with-python/
    ids = ','.join(id_list)
    Entrez.email = 'your.email@example.com'
    handle = Entrez.efetch(db='pubmed',
                           retmode='xml',
                           id=ids)
    results = Entrez.read(handle)
    return results['PubmedArticle']


def parse_paper(paper):
    # based on
    # https://marcobonzanini.com/2015/01/12/searching-pubmed-with-python/
    try:
        paper = flatten(paper)
        title = paper['MedlineCitation_Article_ArticleTitle']
        abstract = paper['MedlineCitation_Article_Abstract_AbstractText']
        if type(abstract) == list:
            abstract = abstract[0]
        authors = [f"{a['LastName']}, {a['Initials']}" for a in flatten(paper)['MedlineCitation_Article_AuthorList']]
        return dict(title=title, authors=authors, abstract=abstract)
    except:
        return None


def dedupe(collection):
    ret = []
    for e in collection:
        if e not in ret:
            ret.append(e)
    return ret


if __name__ == '__main__':
    papers = search('python[title]')
    print(f'Got {len(papers)} papers')
    random.shuffle(papers)
    try:
        classes = json.load(open('paper_classes.json'))
    except:
        classes = dict()

    print('Tag as many papers as you wish. Use "l" for "language", "s" for "snake", "q" to continue')
    for p in papers:
        if p['title'] in classes:
            continue
        tag = input(f"{len(classes) + 1:3d}: {p['title']}")
        if tag.strip() in ('l', 's'):
            classes[p['title']] = tag.strip()
            json.dump(classes, open('paper_classes.json', 'wt'))
        elif tag.strip() == 'q':
            break

    # For easier tagging, we only had papers with the word "python" in the title.
    # Now, let's add some papers with the word in other places
    more_papers = [p for p in search('python', retmax=200) if p not in papers]
    tagged_papers = [p for p in papers if p['title'] in classes]
    untagged_papers = [p for p in papers + more_papers if p not in tagged_papers]
    for paper in tagged_papers:
        tag = classes[paper['title']]
        paper['tag'] = {'s': 'snake', 'l': 'language'}[tag]

    # Let's add more papers using searches that will return papers that belong
    # to one of the tags
    for query in ['python library', 'python framework']:
        print(query)
        tmp = [p for p in search(query, retmax=200)]
        for p in tmp:
            p['tag'] = 'language'
            tagged_papers.append(p)
    for query in ['python venom', 'python heart', 'python pneumonia']:
        print(query)
        tmp = [p for p in search(query, retmax=200)]
        for p in tmp:
            p['tag'] = 'snake'
            tagged_papers.append(p)

    print(f'There are {len(tagged_papers)} tagged and {len(untagged_papers)} untagged papers')
    tagged_papers = dedupe(tagged_papers)
    untagged_papers = dedupe(untagged_papers)
    print(f'There are {len(tagged_papers)} tagged and {len(untagged_papers)} untagged papers')
    json.dump(tagged_papers, open('tagged_papers.json', 'wt'))
    json.dump(untagged_papers, open('untagged_papers.json', 'wt'))
