# -*- coding: utf-8 -*-
"""Stav Grossfeld sniff mac addresses that sent us a packet"""
TIME_TO_STOP = 60

from scapy.all import *



#MY_MAC = "b8:e8:56:2d:cf:06"
def sniffer_2():
    senders = sniff(filter="ether dst host b8:e8:56:2d:cf:06",
          timeout=60, prn=lambda x: x.src)



def sniffer():
    """sniff and print unique mac addresses that sent packets"""

    packets = sniff(timeout=TIME_TO_STOP)

    print packets

    packet_src = []
    for packet in packets:
        if packet.dst == "b8:e8:56:2d:cf:06":
            # print packet.show()
            packet_src.append(packet.src)

    print set(packet_src)


def main():
    """ call sniff function"""
    sniffer_2()

if __name__ == "__main__":
    main()