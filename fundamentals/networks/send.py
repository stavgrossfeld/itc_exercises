"""Stav Grossfeld - send packet to google"""
from scapy.all import *


def send_scapy():
	"""send packet"""

	my_packet = IP(dst = "www.google.com")
	
	my_packet.show()
	
	send(my_packet)

def main():
	"""call send_scapy function"""
	send_scapy()

if __name__ == "__main__":
	main()