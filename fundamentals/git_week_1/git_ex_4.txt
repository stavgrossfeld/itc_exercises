1. clone repo: git clone stavgrossfeld@bitbucket.org:itaiarad/itc.git itai
"""
Cloning into 'itai'...
The authenticity of host 'bitbucket.org (104.192.143.3)' can't be established.
RSA key fingerprint is SHA256:zzXQOXSRBEiUtuE8AikJYKwbHaxvSc0ojez9YXaGp1A.
Are you sure you want to continue connecting (yes/no)? yes
Warning: Permanently added 'bitbucket.org,104.192.143.3' (RSA) to the list of known hosts.
remote: Counting objects: 7, done.
remote: Compressing objects: 100% (6/6), done.
remote: Total 7 (delta 1), reused 0 (delta 0)
Receiving objects: 100% (7/7), done.
Resolving deltas: 100% (1/1), done.
"""

2.  cd itai/
another_file.txt  hello_world.txt

Wed Oct 18 18:08:23 (master)
Stav@Stavs-MacBook-Pro:~/github-repos/itai$ git log
commit 4aea501fd0e78c4f9e651b256c0b864b70454ad4 (HEAD -> master, origin/master, origin/HEAD)
Author: Itay Arad <itayarad@gmail.com>
Date:   Wed Oct 18 16:42:10 2017 +0300

    hello world with changes

commit 88de13fd4a87ee562105fc16a6c6be8ae8351c9f
Author: Itay Arad <itayarad@gmail.com>
Date:   Wed Oct 18 11:19:52 2017 +0300

    ex1 files

Wed Oct 18 18:08:26 (master)
Stav@Stavs-MacBook-Pro:~/github-repos/itai$

3. show revisions by author

 git show
commit 4aea501fd0e78c4f9e651b256c0b864b70454ad4 (HEAD -> master, origin/master, origin/HEAD)
Author: Itay Arad <itayarad@gmail.com>
Date:   Wed Oct 18 16:42:10 2017 +0300

    hello world with changes

diff --git a/hello_world.txt b/hello_world.txt
index fb35ecc..225741b 100644
--- a/hello_world.txt
+++ b/hello_world.txt
@@ -1,6 +1,6 @@
 Lorem ipsum dolor sit amet,
 consectetuer adipiscing elit.
-Aenean commodo ligula eget dolor.
+Aenean commodo changes ligula eget dolor.
 Aenean massa.
 Cum sociis natoque penatibus et magnis dis parturient montes,
 nascetur ridiculus mus.



 4. make changes on B file


Stav@Stavs-MacBook-Pro:~/github-repos/itai$ git add hello_world.txt

Wed Oct 18 18:15:21 (master)
Stav@Stavs-MacBook-Pro:~/github-repos/itai$ git commit -m "stav changes"
[master d7b3e39] stav changes
 1 file changed, 3 insertions(+), 3 deletions(-)

 git push
Warning: Permanently added the RSA host key for IP address '104.192.143.1' to the list of known hosts.
Counting objects: 3, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (3/3), 351 bytes | 351.00 KiB/s, done.
Total 3 (delta 1), reused 0 (delta 0)
To bitbucket.org:itaiarad/itc.git
   4aea501..d7b3e39  master -> master
