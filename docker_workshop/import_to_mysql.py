import pymysql.cursors
import csv
import json
import time
from geopy.distance import vincenty
import argparse
import sys

FILE_PATH = '/Users/ilaif/Desktop/data/bi-workshop/train.csv'
CHUNK_SIZE = 5000
START_TIMESTAMP = 1398902400
TRIPS_COUNT = 315184  # Currently not used
RECREATE_SCHEMA = True
LOAD_BASE_TABLES = True
UPDATE_TIMESTAMPS = True
CALCULATE_STANDS_LOC = True
LOAD_ROUTES = False  # Should remain false!
DB_HOST = 'localhost'
DB_USER = 'root'
DB_PASS = 'superset'
DB_PORT = 3307

cols = ["TRIP_ID", "CALL_TYPE", "ORIGIN_CALL", "ORIGIN_STAND", "TAXI_ID", "TIMESTAMP", "DAY_TYPE", "MISSING_DATA",
        "POLYLINE"]
TRIP_ID = "TRIP_ID"
CALL_TYPE = "CALL_TYPE"
ORIGIN_CALL = "ORIGIN_CALL"
ORIGIN_STAND = "ORIGIN_STAND"
TAXI_ID = "TAXI_ID"
TIMESTAMP = "TIMESTAMP"
DAY_TYPE = "DAY_TYPE"
MISSING_DATA = "MISSING_DATA"
POLYLINE = "POLYLINE"
LAT = 'latitude'
LNG = 'longitude'
VAR = 'variance'
MEAN = 'mean'
N = 'n'
M2 = 'M2'

LETTER_TO_INT = {
    'A': 0,
    'B': 1,
    'C': 2
}

STRING_TO_BOOL = {
    'False': 0,
    'True': 1
}


def main():
    start_time = time.time()

    con = pymysql.connect(host=DB_HOST,
                          user=DB_USER,
                          password=DB_PASS,
                          port=DB_PORT,
                          charset='utf8mb4',
                          cursorclass=pymysql.cursors.DictCursor)

    try:
        if RECREATE_SCHEMA:
            create_schema(con)
        if LOAD_BASE_TABLES:
            load_base_tables(con)
        if LOAD_ROUTES:
            load_routes(con)
        if UPDATE_TIMESTAMPS:
            update_timestamps(con)
        if CALCULATE_STANDS_LOC:
            calculate_stands_loc(con)

    finally:
        print('Closing db connection...')
        con.close()

    minutes_passed = round((time.time() - start_time) / 60, 2)
    print('Importing to mysql took %s minutes.' % (minutes_passed,))


""""""""""""""""""""
""" Main Methods """
""""""""""""""""""""


def create_schema(con):
    """
    Create main schema
    :param con:
    :return:
    """
    print('Recreating schema (DELETING FORMER DATA)...')
    sql = """
        DROP DATABASE IF EXISTS taxi;
        CREATE DATABASE taxi;

        USE taxi;

        CREATE TABLE taxi.clients
        (
            id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
            external_id VARCHAR(45) NOT NULL
        );
        CREATE UNIQUE INDEX clients_external_id_uindex ON taxi.clients (external_id);

        CREATE TABLE taxi.stands
        (
            id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
            external_id VARCHAR(45) NOT NULL,
            approx_latitude FLOAT(10,6) NULL,
            approx_longitude FLOAT(10,6) NULL
        );
        CREATE UNIQUE INDEX stands_external_id_uindex ON taxi.stands (external_id);

        CREATE TABLE taxi.taxis
        (
            id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
            external_id VARCHAR(45) NOT NULL
        );
        CREATE UNIQUE INDEX taxis_external_id_uindex ON taxis (external_id);

        CREATE TABLE taxi.trips
        (
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            external_id VARCHAR(45) NOT NULL,
            call_type TINYINT NOT NULL,
            client_id INT,
            stand_id INT,
            taxi_id INT,
            `timestamp` INT,
            day_type TINYINT,
            missing_data BOOLEAN,
            start_latitude FLOAT(10,6) NULL,
            start_longitude FLOAT(10,6) NULL,
            end_latitude FLOAT(10,6) NULL,
            end_longitude FLOAT(10,6) NULL,
            duration_seconds INTEGER NULL,
            approx_distance_meters INTEGER NULL,
            CONSTRAINT trips_clients_id_fk FOREIGN KEY (client_id) REFERENCES clients (id),
            CONSTRAINT trips_stands_id_fk FOREIGN KEY (stand_id) REFERENCES stands (id),
            CONSTRAINT trips_taxis_id_fk FOREIGN KEY (taxi_id) REFERENCES taxis (id)
        );
        CREATE UNIQUE INDEX trips_external_id_uindex ON taxi.trips (external_id);
        CREATE INDEX trips_timestamp_index ON taxi.trips (`timestamp`);

        CREATE TABLE taxi.routes
        (
            id INT PRIMARY KEY NOT NULL AUTO_INCREMENT,
            latitude FLOAT(10,6) NOT NULL,
            longitude FLOAT(10,6) NOT NULL,
            trip_id INT NOT NULL,
            CONSTRAINT routes_trips_id_fk FOREIGN KEY (trip_id) REFERENCES trips (id)
        );
    """
    with con.cursor() as cursor:
        cursor.execute(sql)
        con.commit()
    print('Created schema successfully')


def load_base_tables(con):
    print('Loading base tables...')
    clients = {}
    clients_cou = 1
    stands = {}
    stands_cou = 1
    taxis = {}
    taxis_cou = 1
    trips = {}
    trips_cou = 1

    with open(FILE_PATH) as f:
        reader = csv.DictReader(f)
        for r in reader:

            timestamp = int(r[TIMESTAMP])
            if timestamp < START_TIMESTAMP:  # Get only last 2 months from the dataset
                continue

            if r[ORIGIN_CALL] == '':
                r[ORIGIN_CALL] = 'NULL'
            if r[ORIGIN_CALL] not in clients:
                clients[r[ORIGIN_CALL]] = {
                    'external_id': r[ORIGIN_CALL], 'id': clients_cou}
                clients_cou += 1
            client_id = clients[r[ORIGIN_CALL]]['id']

            if r[ORIGIN_STAND] == '':
                r[ORIGIN_STAND] = 'NULL'
            if r[ORIGIN_STAND] not in stands:
                stands[r[ORIGIN_STAND]] = {
                    'external_id': r[ORIGIN_STAND], 'id': stands_cou}
                stands_cou += 1
            stand_id = stands[r[ORIGIN_STAND]]['id']

            if r[TAXI_ID] not in taxis:
                taxis[r[TAXI_ID]] = {
                    'external_id': r[TAXI_ID], 'id': taxis_cou}
                taxis_cou += 1
            taxi_id = taxis[r[TAXI_ID]]['id']

            start_longitude = start_latitude = None
            end_longitude = end_latitude = None
            duration_seconds = None
            approx_distance_meters = None
            route_data = json.loads(r[POLYLINE])
            if len(route_data) > 0:
                start_longitude, start_latitude = route_data[0]  # First point
                end_longitude, end_latitude = route_data[-1]  # Last point
                # Coordinates are reported every 15 seconds
                duration_seconds = len(route_data) * 15
                # Approx distance by taking distance between coordinates
                last_longitude = start_longitude
                last_latitude = start_latitude
                approx_distance_meters = 0
                for i in range(1, len(route_data)):
                    longitude, latitude = route_data[i]
                    start = (last_latitude, last_longitude)
                    end = (latitude, longitude)
                    approx_distance_meters += vincenty(start, end).meters
                    last_longitude = longitude
                    last_latitude = latitude
                approx_distance_meters = int(approx_distance_meters)

            if r[TRIP_ID] not in trips:
                trips[r[TRIP_ID]] = {'external_id': r[TRIP_ID], 'call_type': LETTER_TO_INT[r[CALL_TYPE]],
                                     'client_id': client_id, 'taxi_id': taxi_id, 'stand_id': stand_id,
                                     'time_stamp': timestamp, 'day_type': LETTER_TO_INT[r[DAY_TYPE]],
                                     'missing_data': STRING_TO_BOOL[r[MISSING_DATA]], 'id': trips_cou,
                                     'start_longitude': start_longitude, 'start_latitude': start_latitude,
                                     'end_longitude': end_longitude, 'end_latitude': end_latitude,
                                     'duration_seconds': duration_seconds,
                                     'approx_distance_meters': approx_distance_meters}
                trips_cou += 1

            if trips_cou % CHUNK_SIZE == 0:
                print('Processed %s records' % (trips_cou,))

    with con.cursor() as cursor:
        insert_data_to_tables(cursor, con, taxis, clients, stands, trips)

    print('Loaded base tables successfully')


def load_routes(con):
    print('Loading routes...')
    trips = {}
    trips_cou = 1
    routes = []
    routes_cou = 1

    sql = """
        INSERT INTO taxi.`routes` (`id`, `trip_id`, `latitude`, `longitude`)
        VALUES (%(id)s, %(trip_id)s, %(latitude)s, %(longitude)s)
    """
    with con.cursor() as cursor:
        with open(FILE_PATH) as f:
            reader = csv.DictReader(f)
            for r in reader:
                # Get only last 2 months from the dataset
                if int(r[TIMESTAMP]) < START_TIMESTAMP:
                    continue

                route_data = json.loads(r[POLYLINE])

                if r[TRIP_ID] not in trips:
                    trips[r[TRIP_ID]] = {}  # TODO: Change to set?
                    trips_cou += 1

                for r_pair in route_data:
                    route = {'trip_id': trips_cou - 1,
                             LNG: r_pair[0], LAT: r_pair[1], 'id': routes_cou}
                    routes.append(route)
                    routes_cou += 1

                    if routes_cou % CHUNK_SIZE == 0:
                        executemany(cursor, con, sql, routes)
                        routes = []
                        print('Processed and loaded %s routes' % (routes_cou,))

            # Finally clean the rest of the routes
            executemany(cursor, con, sql, routes)
            del routes

    print('Loaded routes successfully')


def update_timestamps(con):
    print('Updating timestamps...')
    sql = """
        SET @seconds_add = (SELECT (datediff(now(), from_unixtime(max(TIMESTAMP))) - 1) * 24 * 60 * 60 FROM taxi.trips);
        UPDATE taxi.trips SET `timestamp` = `timestamp` + @seconds_add;
    """
    print('Updating timestamps to NOW')
    with con.cursor() as cursor:
        cursor.execute(sql)
        con.commit()
    print('Updated timestamps successfully')


def calculate_stands_loc(con):
    print('Calculating stands approximate locations...')
    cou = 1
    stands = {}
    stands_cou = 1  # Start from two since we have some null stands
    stand_locs = {}

    with open(FILE_PATH) as f:
        reader = csv.DictReader(f)
        for r in reader:
            # Get only last 2 months from the dataset
            # And, we want to skip trips which do not have a stand
            if int(r[TIMESTAMP]) < START_TIMESTAMP:
                continue

            route_data = json.loads(r[POLYLINE])

            if r[ORIGIN_STAND] not in stands:
                stands[r[ORIGIN_STAND]] = {'id': stands_cou}
                stands_cou += 1
            stand_id = stands[r[ORIGIN_STAND]]['id']

            if stand_id not in stand_locs:  # Init for new stands
                stand_locs[stand_id] = {N: 0,
                                        LAT: {MEAN: 0.0, M2: 0.0},
                                        LNG: {MEAN: 0.0, M2: 0.0}
                                        }

            if len(route_data) > 0:
                longitude, latitude = route_data[0]
                stand_locs[stand_id][N] += 1
                online_variance_step(
                    float(latitude), stand_locs[stand_id][N], stand_locs[stand_id][LAT])
                online_variance_step(
                    float(longitude), stand_locs[stand_id][N], stand_locs[stand_id][LNG])

            if cou % CHUNK_SIZE == 0:
                print('Processed %s trips' % (cou,))

            cou += 1

    for stand_id, stats in stand_locs.items():
        if stats[N] < 2:
            stats[LAT][VAR] = float('nan')
            stats[LNG][VAR] = float('nan')
        else:
            stats[LAT][VAR] = round(stats[LAT][M2] / (stats[N] - 1), 4)
            stats[LAT][MEAN] = round(stats[LAT][MEAN], 4)
            del stats[LAT][M2]
            stats[LNG][VAR] = round(stats[LNG][M2] / (stats[N] - 1), 4)
            stats[LNG][MEAN] = round(stats[LNG][MEAN], 4)
            del stats[LNG][M2]

            # if stats[LNG][VAR] > 0.00001 or stats[LAT][VAR] > 0.00001:
            #     print((stand_id, stats))

    with con.cursor() as cursor:
        sql = """
        UPDATE taxi.`stands` SET `approx_latitude` = %s, `approx_longitude` = %s WHERE `id` = %s;
        """
        data = [(stats[LAT][MEAN], stats[LNG][MEAN], stand_id)
                for stand_id, stats in stand_locs.items()]
        executemany(cursor, con, sql, data)

    print('Loaded approximate stand locations successfully')


""""""""""""""""""""
""" Subroutines  """
""""""""""""""""""""


def online_variance_step(x, n, container):
    delta = x - container[MEAN]
    container[MEAN] += delta / n
    container[M2] += delta * (x - container[MEAN])


def insert_data_to_tables(cursor, con, taxis, clients, stands, trips):
    sql = "INSERT INTO taxi.`taxis` (`id`, `external_id`) VALUES (%(id)s, %(external_id)s)"
    executemany(cursor, con, sql, list(taxis.values()))
    print('Inserted taxis')

    sql = "INSERT INTO taxi.`clients` (`id`, `external_id`) VALUES (%(id)s, %(external_id)s)"
    executemany(cursor, con, sql, list(clients.values()))
    print('Inserted clients')

    sql = "INSERT INTO taxi.`stands` (`id`, `external_id`) VALUES (%(id)s, %(external_id)s)"
    executemany(cursor, con, sql, list(stands.values()))
    print('Inserted stands')

    sql = """
        INSERT INTO taxi.`trips` (`id`, `external_id`, `call_type`, `client_id`, `stand_id`, `taxi_id`,
        `timestamp`, `day_type`, `missing_data`, `start_longitude`, `start_latitude`, `end_longitude`,
        `end_latitude`, `duration_seconds`, `approx_distance_meters`)
        VALUES (%(id)s, %(external_id)s, %(call_type)s, %(client_id)s, %(stand_id)s, %(taxi_id)s,
        %(time_stamp)s, %(day_type)s, %(missing_data)s, %(start_longitude)s, %(start_latitude)s,
        %(end_longitude)s, %(end_latitude)s, %(duration_seconds)s, %(approx_distance_meters)s)
    """

    executemany(cursor, con, sql, list(trips.values()))
    print('Inserted trips')


""""""""""""""""""""""""
"""  Helper Methods  """
""""""""""""""""""""""""


def executemany(cursor, con, query, values):
    cou = 0
    for chunk in chunks(values, CHUNK_SIZE):
        cursor.executemany(query, chunk)
        cou += len(chunk)
        con.commit()
        print('Inserted %s records' % (cou,))


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Short sample app')

    parser.add_argument('--source', action="store",
                        dest='source', default=FILE_PATH)
    parser.add_argument('--host', action="store", dest='host', default=DB_HOST)
    parser.add_argument('--user', action="store", dest='user', default=DB_USER)
    parser.add_argument('--pass', action="store",
                        dest='password', default=DB_PASS)
    parser.add_argument('--port', action="store",
                        dest='port', type=int, default=DB_PORT)

    args = parser.parse_args(sys.argv[1:])

    FILE_PATH = args.source
    DB_HOST = args.host
    DB_USER = args.user
    DB_PASS = args.password
    DB_PORT = args.port

    main()
