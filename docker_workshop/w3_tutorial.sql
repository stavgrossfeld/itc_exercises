https://github.com/idomintz1/Advanced-sql-for-ds/blob/master/Advanced%20SQL%20for%20DS.sql


1. For each week and employee , what is the rolling average number of orders?


WITH orders_2 AS (
  SELECT  *,  EmployeeID,
          date(strftime('%Y-%m-%d',orderdate), 'weekday 1') AS week
    FROM [orders]
)

,
ct_weeks AS 
  (
  SELECT w.week AS week,
         w.EmployeeID AS EmployeeID,
         count(o.orderid) AS ct_orders
    FROM orders_2 w
    JOIN orders_2 o
      ON w.week >= o.week
  GROUP BY  w.week, w.EmployeeID
  ORDER BY 1
  ) 


SELECT 
  w.week,
  w.EmployeeID,
  avg(w.ct_orders) AS moving_avg
FROM 
  ct_weeks w 
JOIN 
  ct_weeks o 
ON 
  w.week >= o.week
GROUP BY 
  w.week, w.EmployeeID
ORDER BY 
  1

### not together
SELECT 
  w.week,
  avg(w.ct_orders) AS moving_avg
FROM 
  ct_weeks w 
JOIN 
  ct_weeks o 
ON 
  w.week >= o.week
GROUP BY 
  w.week
ORDER BY 
  1


SELECT 
  w.EmployeeID,
  avg(w.ct_orders) AS moving_avg
FROM 
  ct_weeks w 
JOIN 
  ct_weeks o 
ON 
  w.week >= o.week
GROUP BY 
  w.EmployeeID
ORDER BY 
  1

2. What was the value of order 10248?
3. For each week, what is the revenue?
4. For each week, what is the total revenue earned until that week?
5. For each week, what is the rolling average revenue of the last month?
6. For each week and employee, what is the value of last order?


with orders as (
SELECT 
  EmployeeId as employee,
  date(strftime('%Y-%m-%d',orderdate), 'weekday 1') AS week,
  OrderID
FROM 
  orders)

SELECT
  *
FROM 
  orders o
LEFT JOIN 
  orders w
ON 
  o.week = w.week

        