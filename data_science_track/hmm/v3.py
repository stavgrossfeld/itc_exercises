import random
import numpy as np
from numpy.random import choice

# OBSERVATIONS = ('normal', 'cold', 'dizzy')
# STATES = ('Healthy', 'Fever')
# START_P = {'Healthy': 0.6, 'Fever': 0.4}
# TRANS_P = {
#    'Healthy' : {'Healthy': 0.7, 'Fever': 0.3},
#    'Fever' : {'Healthy': 0.4, 'Fever': 0.6}
#    }
# EMIT_P = {
#    'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
#    'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
#    }


OBSERVATIONS = ("clean", "dirty")
STATES = ("sun", "rain")
START_P = {"sun" : 0.5, "rain" : 0.5}
TRANS_P = {
			"sun" : {"sun":.6 , "rain":.4},
			"rain": {"sun":.3, "rain": .7}
		  }
EMIT_P = {
			"sun" : {"clean":.4 , "dirty":.6},
			"rain": {"clean":.1, "dirty": .9}
		  }
class hmm():
	def __init__(self, start_p, emit_p, trans_p, states, obs):
		self
		self.emit_p = emit_p
		self.trans_p = trans_p
		self.states = states
		self.obs = obs
		self.start_p = start_p

	def viterbi(self,observations):
		n = len(observations)
		states = self.states
		v = np.zeros((n,len(states)))

		v_path = np.zeros((n,len(states)), dtype = object)

		# viterbi t = 0
		for st_ix, st in enumerate(states):
			init_ob = observations[0]
			v[0,st_ix] = self.emit_p[st][init_ob] * self.start_p[st]
			v_path[0,st_ix] = 1

		# run viterbi for t > 1
		for t in range(1, n):
			current_obs = observations[t]

			for st_ix_2, st_2 in enumerate(states):

				prob = v[t-1] * self.trans_p[st_2].values() * self.emit_p[st_2][current_obs] # for prev_st in states]
				v[t, st_ix_2] = np.max(prob)
				v_path[t,st_ix_2] = np.argmax(prob)

		hidden = np.zeros(n)
		hidden[n-1] = np.argmax(v[n-1])
		for t_2 in range(n-2,-1,-1):
			hidden[t_2] = int(v_path[t_2+1, int(hidden[t_2+1])])



		print "v:" , v
		print "\n path:", v_path

		return [states[int(hid)] for hid in hidden[::-1]]


def main():
	model = hmm(START_P, EMIT_P, TRANS_P, STATES, OBSERVATIONS)

	#print model.viterbi(["normal","cold","dizzy"])
	print model.viterbi(["dirty","clean","clean","dirty","dirty","dirty"], "dirty","clean","dirty","dirty", "dirty"])

main()