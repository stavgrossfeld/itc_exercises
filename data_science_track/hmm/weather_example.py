
OBSERVATIONS = ("clean", "dirty")
STATES = ("sun", "rain")
START_P = {"sun" : 0.5, "rain" : 0.5}
TRANS_P = {
			"sun" : {"sun":.6 , "rain":.4},
			"rain": {"sun":.3, "rain": .7}
		  }
EMIT_P = {
			"sun" : {"clean":.4 , "dirty":.6},
			"rain": {"clean":.1, "dirty": .9}
		  }


#A = [[0.6, 0.4], [0.3, 0.7]]  # sun = 0 rain = 1
#B = [[0.4,0.6], [0.1,0.9]]	  # clean = 0 dirty = 1



OBSERVATIONS = ('normal', 'cold', 'dizzy')
STATES = ('Healthy', 'Fever')
START_P = {'Healthy': 0.6, 'Fever': 0.4}
TRANS_P = {
   'Healthy' : {'Healthy': 0.7, 'Fever': 0.3},
   'Fever' : {'Healthy': 0.4, 'Fever': 0.6}
   }
EMIT_P = {
   'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
   'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
   }