import random
import numpy as np
from numpy.random import choice

# OBSERVATIONS = ('normal', 'cold', 'dizzy')
# STATES = ('Healthy', 'Fever')
# START_P = {'Healthy': 0.6, 'Fever': 0.4}
# TRANS_P = {
#    'Healthy' : {'Healthy': 0.7, 'Fever': 0.3},
#    'Fever' : {'Healthy': 0.4, 'Fever': 0.6}
#    }
# EMIT_P = {
#    'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
#    'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
#    }

OBSERVATIONS = ("clean", "dirty")
STATES = ("sun", "rain")
START_P = {"sun" : 0.3, "rain" : 0.7}
TRANS_P = {
			"sun" : {"sun":.6 , "rain":.4},
			"rain": {"sun":.3, "rain": .7}
		  }
EMIT_P = {
			"sun" : {"clean":.4 , "dirty":.6},
			"rain": {"clean":.1, "dirty": .9}
		  }
class hmm():
	def __init__(self, start_p, emit_p, trans_p, states, obs):
		self
		self.emit_p = emit_p
		self.trans_p = trans_p
		self.states = states
		self.obs = obs
		self.start_p = start_p

	def viterbi(self,observations):
		n = len(observations)
		states = self.states
		v = np.zeros((n,len(states)))

		v_path = np.empty((n,len(states)), dtype = object)

		v_path.fill([])

		# viterbi t = 0
		for st_ix, st in enumerate(states):
			init_ob = observations[0]
			v[0,st_ix] = self.emit_p[st][init_ob] * self.start_p[st]
			v_path[0,st_ix] = 1

		# run viterbi for t > 1
		for t in range(1, n):
			current_obs = observations[t]

			for current_st_ix, current_st in enumerate(states):

				#prob = v[t-1][current_st_ix] * self.trans_p[prev_st][current_st] * self.emit_p[current_st][current_obs]

				prob_arr = []

				prob

				for prev_st in states:

					prob = v[t-1][current_st_ix] * self.trans_p[prev_st][current_st] * self.emit_p[current_st][current_obs]

					prob_arr.append((prob, prev_st))

				#v_path[t,current_st_ix] = np.argmax(v[t-1] * self.trans_p[current_st].values())

				max_prob_arr = max(prob_arr)

				max_prob, max_path = max_prob_arr[0], max_prob_arr[1]

				v[t,current_st_ix] = max_prob

		print v
		print v_path

		hidden = np.zeros(n)

		hidden[n-1] = np.argmax(v[n-1])

		for t_2 in range(n-2,-1,-1):
			hidden[t_2] = v_path[t_2+1,int(hidden[t_2+1])]

		print hidden

def main():
	model = hmm(START_P, EMIT_P, TRANS_P, STATES, OBSERVATIONS)

	#model.viterbi(["normal","cold","dizzy"])
	model.viterbi(["clean","dirty","clean","clean","dirty","dirty", "dirty","clean","dirty","dirty", "dirty"])

main()