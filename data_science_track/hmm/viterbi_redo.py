import random
import numpy as np
from numpy.random import choice

# OBSERVATIONS = ("clean", "dirty")
# STATES = ("sun", "rain")
# START_P = {"sun" : 0.5, "rain" : 0.5}
# TRANS_P = {
# 			"sun" : {"sun":.6 , "rain":.4},
# 			"rain": {"sun":.3, "rain": .7}
# 		  }
# EMIT_P = {
# 			"sun" : {"clean":.4 , "dirty":.6},
# 			"rain": {"clean":.1, "dirty": .9}
# 		  }


OBSERVATIONS = ('normal', 'cold', 'dizzy')
STATES = ('Healthy', 'Fever')
START_P = {'Healthy': 0.6, 'Fever': 0.4}
TRANS_P = {
   'Healthy' : {'Healthy': 0.7, 'Fever': 0.3},
   'Fever' : {'Healthy': 0.4, 'Fever': 0.6}
   }
EMIT_P = {
   'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
   'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
   }




class hmm():
	def __init__(self, start_p, emit_p, trans_p, states, obs):
		self
		self.emit_p = emit_p
		self.trans_p = trans_p
		self.states = states
		self.obs = obs

	def viterbi(self,observations):
		n = len(observations)
		states = self.states

		v = np.zeros((n,len(states)))
		v2 = np.zeros((n, len(states)))
		path = []

		for k in range(n): # iterate over days
			#print k
			current_obs = observations[k]
			for index, current_state in enumerate(states):   # iterate over current state

			# initialize
				if k < 1:
					v[k,index] = self.emit_p[current_state][current_obs] * START_P[current_state] # "obs|hidden * initial"

				else:
					prob_arr = []
					for last_state in states:   # iterate over possible states aka last state
						path_prob = v[k-1,index] * self.trans_p[last_state][current_state] * self.emit_p[current_state][current_obs]   # multiply last probability by transition probability
						prob_arr.append((path_prob, current_state))


					v[k,index] = max(prob_arr)[0]
					path.append(max(prob_arr[1]))
		print v
		print path
		#print [states[np.argmax(i)] for i in v]


def main():
	model = hmm(START_P, EMIT_P, TRANS_P, STATES, OBSERVATIONS)
	#model.viterbi(["clean","clean","clean","dirty","dirty","clean","clean","dirty","dirty"])
	#model.viterbi(["clean","dirty","clean", "dirty","dirty", "dirty"])


	#model.viterbi(["clean","dirty","clean"])

	model.viterbi(["normal","cold","dizzy"])

main()