"""1. Write a class HMM that has transition
probabilities, emission probabilities, names for each
state and output."""
"""class that executes hmm"""
import random
import numpy as np
from numpy.random import choice

OBSERVATIONS = ('normal', 'cold', 'dizzy')
STATES = ('Healthy', 'Fever')
START_P = {'Healthy': 0.6, 'Fever': 0.4}
TRANS_P = {
   'Healthy' : {'Healthy': 0.7, 'Fever': 0.3},
   'Fever' : {'Healthy': 0.4, 'Fever': 0.6}
   }
EMIT_P = {
   'Healthy' : {'normal': 0.5, 'cold': 0.4, 'dizzy': 0.1},
   'Fever' : {'normal': 0.1, 'cold': 0.3, 'dizzy': 0.6}
   }

# OBSERVATIONS = ("clean", "dirty")
# STATES = ("sun", "rain")
# START_P = {"sun" : 0.5, "rain" : 0.5}
# TRANS_P = {
#           "sun" : {"sun":.6 , "rain":.4},
#           "rain": {"sun":.3, "rain": .7}
#         }
# EMIT_P = {
#           "sun" : {"clean":.4 , "dirty":.6},
#           "rain": {"clean":.1, "dirty": .9}
#         }


class hmm():
    def __init__(self, start_p, emit_p, trans_p, states, obs):
        self
        self.emit_p = emit_p
        self.trans_p = trans_p
        self.states = states
        self.obs = obs
        self.start_p = start_p

    def generate(self, length):
        """(a) Add a `generate` function which takes a
    length as input and outputs names of states
    and observable values."""

        generated_states, generated_obs = [], []

        last_draw =  choice(START_P.keys(), 1, p = START_P.values())[0]

        for i in np.arange(length):
            # transit probabilities of last state, emit probabilities of last state
            current_state, current_obs = self.trans_p[last_draw], self.emit_p[last_draw]

            state_draw, obs_draw = choice(current_state.keys(), 1, p = current_state.values())[0], choice(current_obs.keys(), 1, p = current_obs.values())[0]

            last_draw = state_draw # update current state

            generated_states.append(state_draw)
            generated_obs.append(obs_draw)

        return generated_states, generated_obs

    def viterbi(self,observations):
        n = len(observations)
        states = self.states
        v = np.zeros((n,len(states)))

        v_path = np.zeros((n,len(states)), dtype = object)

        # viterbi t = 0
        for st_ix, st in enumerate(states):
            init_ob = observations[0]
            v[0,st_ix] = self.emit_p[st][init_ob] * self.start_p[st]
            v_path[0,st_ix] = 1

        # run viterbi for t > 1
        for t in range(1, n):
            current_obs = observations[t]
            for st_ix_2, st_2 in enumerate(states):

                prob_arr = [(v[t-1][prev_st] * self.trans_p[states[prev_st]][st_2] * self.emit_p[st_2][current_obs], prev_st) for prev_st in range(len(states))]

                v[t, st_ix_2] = max(prob_arr, key = lambda val: val[0])[0]
                v_path[t,st_ix_2] = max(prob_arr, key = lambda val: val[0])[1]

        hidden = np.zeros(n)
        hidden[n-1] = np.argmax(v[n-1])
        for t_2 in range(n-2,-1,-1):
            hidden[t_2] = int(v_path[t_2+1, int(hidden[t_2+1])])

        print sum(hidden)
        return [states[int(h)] for h in hidden]



    def backward_forward(self,observations):
        """(c) Implement `backward_forward` algorithm
    which outputs the probabilties of each state
    for a given observation."""
        # compute joint distributions (Z[k], x[1:k])
        n = len(observations)


        states = self.states

        # intiitalize
        fwd = []
        f_prev = {}


        for obs_ix, obs in enumerate(observations):
            f_curr = {}
            for st_ix, st in enumerate(states):
                if obs_ix == 0:
                    prev_f_sum = self.start_p[st]
                else:
                    prev_f_sum = sum(f_prev[prev_st]*self.trans_p[prev_st][st] for prev_st in states)

                    print "state", st, prev_f_sum


                print "current_obs", obs, f_prev

                f_curr[st] = self.emit_p[st][obs] * prev_f_sum
            fwd.append(f_curr)
            f_prev = f_curr

        print fwd
        print f_prev




    def baum_welch(self, probabilities):
        """Implement the Baum-Welch algorithm. Use the
        class transmissions and emission probabilities as
        prior. Call this function learn. Add stop criterion as
        parameters with default values."""

def main():
    """Try your class with the transition probabilites in the
    weather model in a previous presentation.
    Generate 10 sequences of length 10. Find their most
    likely states. Are they the same?"""

    #emit_p, trans_p, states, observations
    model = hmm(START_P, EMIT_P, TRANS_P, STATES, OBSERVATIONS)

    # states_arr = []
    # obs_arr = []
    # model.generate(10)
    # for i in range(10):
    #   states, obs = model.generate(10)
    #   states_arr.append(states)
    #   obs_arr.append(obs)

    #print model.viterbi(["normal","cold","dizzy"])

    model.backward_forward(["normal","cold","dizzy"])


    #print model.viterbi(obs_arr[0])
    # for item in range(10):
    #   print states_arr[item]
    #   print obs_arr[item]
    #   print model.viterbi(obs_arr[item])
    #   print "\n"
    #   #print ["sun" if i == 0 else "rain" for i in weather[0]] ,  "\n", ["clean" if i == 0 else "dirty" for i in weather[1]]

    #   states.append(weather)

    # print weather


if __name__ == "__main__":
    main()