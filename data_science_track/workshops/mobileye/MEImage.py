import numpy as np
from matplotlib import pyplot as plt

class MEImage():
    def __init__(self, allocated_size=None, origin=None, image_rect=None):
        if (allocated_size==None):
            allocated_size = [1, 1]

        self._origin = origin;
        self._allocated_size = allocated_size;
        self._image_rect = image_rect;

        self.im = np.ndarray(shape=(allocated_size[0],allocated_size[1]), dtype=int)






    def from_file(self, file_name, as_type=None):
        with open(file_name, 'r') as f:
            sz = np.fromfile(f, count=2, sep=' ').astype('int')
            self._origin = np.fromfile(f, count=2, sep=' ').astype('int')
            self._image_rect = np.fromfile(f, count=4, sep=' ').astype('int')
            self.im = np.fromfile(f, sep=' ').reshape(sz).astype('int')
            self._allocated_size = self.im.shape[::-1]
            if as_type is not None:
                self.im = self.im.astype(as_type)
        return self

    def to_file(self, file_name):
        with open(file_name, 'w') as f:
            np.array(self.im.shape).tofile(f, sep=' ')
            f.write(' ')
            self._origin.tofile(f, sep=' ')
            f.write(' ')
            self._image_rect.tofile(f, sep=' ')
            f.write(' ')
            self.im.tofile(f, sep=' ')




