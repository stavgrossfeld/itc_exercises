import urllib.request
import numbers
import numpy as np
SERVER_URL = 'http://18.196.126.96:8000/evaluate'
EXPECTED_NUM_PREDICTIONS = 102136

predictions_list = np.load("stav_new_test.npy")
print (len(predictions_list))
print(type(predictions_list))
def submit(predictions_list, team_ID):
    #assert len(predictions_list) == EXPECTED_NUM_PREDICTIONS
    for y in predictions_list:
        isinstance(y, numbers.Real)

    print('Preparing submission')
    Y_hat_csv = ','.join(map(str, predictions_list))
    post_data = urllib.parse.urlencode({'Y_hat_csv': Y_hat_csv, 'team_ID': team_ID}).encode()
    print('Submitting')
    req = urllib.request.Request(SERVER_URL, post_data)
    with urllib.request.urlopen(req) as response:
        text_response = response.read()
    return float(text_response)

submit(predictions_list, "America 8=D  France")