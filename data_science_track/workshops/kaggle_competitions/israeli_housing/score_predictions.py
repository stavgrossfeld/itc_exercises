from numpy import asarray, mean, absolute

def score_mrad(Y, Y_predicted):
    Y = asarray(Y, dtype=float)
    Y_predicted = asarray(Y_predicted, dtype=float)

    return mean(absolute(Y-Y_predicted)/Y)
