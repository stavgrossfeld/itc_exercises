#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int is_integer(char number[]);

int main(int argc, char **argv)
{
    int sum=0; 
    int n = 5;
    char str[1024];
    str[0] = '\0';
    char* end;
    int i;
    for (i = 1; i < argc; i++) {
        if ((is_integer(argv[i]) == 1) && (i <=n)) {
            sum += strtol(argv[i], &end, 10);
        }
        else {
        	if (i < n) {
                printf("Not an integer %s in position %d, but should be 5 integers\n", argv[i], i);        		
                return 0;
        	}
        	else {
            	if (i > 6) {
                	strcat(str, " ");
            	}
            	strcat(str, argv[i]);
        	}
        }
    }
    printf("%d \n",sum);
    int len = strlen(str);
    
    int my_index = len;

    for (i=0;i<=len;i++)
    {
    	printf("%c",str[my_index]);
    	my_index--;
    }
    return 0;
}

int is_integer(char inputval[]) {
    char* end;
    strtol(inputval, &end, 10);
    if ((end == inputval) || (*end != '\0')) {
        return 0;
    } 
    return 1;
}