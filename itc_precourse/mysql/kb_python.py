# kb_python.py

#pip install pymysql

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('white')
# %matplotlib inline


sns.set(rc={"figure.figsize": (12, 12)})
import pandas as pd
import pymysql
from sqlalchemy import create_engine

engine = create_engine('mysql+pymysql://root@localhost/imdb')

qry = qry = """
SELECT
  COUNT(DISTINCT(actor_1)) AS kb,
  COUNT(DISTINCT(actor_2)) AS one,
  COUNT(DISTINCT(actor_3)) AS two,
  COUNT(DISTINCT(actor_4)) AS three
FROM 
  kb_4
"""
df = pd.read_sql_query(qry, engine)
df.head()

df = pd.read_sql_query(qry, engine)
df.head()


df_t = df.transpose()
df_t.head()

df_t = df_t.reset_index()

df_t.columns = ['degree','ct']

ax = sns.barplot(x="degree", y="ct", hue="degree", data=df_t); 

ax.set_ylabel('Count of actors')
ax.set_title('Kevin Bacon Degrees of Separation')
plt.savefig('kevin_bacon.png')
