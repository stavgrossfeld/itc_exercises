1. What are the full names of the actors in the DB? Order them alphabetically
SELECT
  DISTINCT(full_name) AS full_name
FROM
  imdb.actors
ORDER BY 
  full_name;

2. How many movies are there? 

SELECT 
   COUNT(*) 
 FROM 
   imdb.movies;
 

3. How many action movies are there? 

 SELECT 
  COUNT(*) 
 FROM 
 	imdb.movies 
 WHERE 
 	genre = 'Action';
+----------+
| COUNT(*) |
+----------+
|        3 |
+----------+


4. How many non-action movies are there?

 SELECT 
  COUNT(*)
FROM 
  imdb.movies
WHERE 
  genre != 'Action' or genre IS NULL;


+----------+
| COUNT(*) |
+----------+
|       35 |
+----------+



5. How many movies came out in each year?

SELECT
  COUNT(*),
  year
FROM 
  imdb.movies
GROUP BY
  year
ORDER BY
  year

+----------+------+
| COUNT(*) | year |
+----------+------+
|        1 | 1972 |
|        1 | 1977 |
|        1 | 1978 |
|        1 | 1983 |
|        1 | 1984 |
|        1 | 1986 |
|        1 | 1987 |
|        2 | 1989 |
|        1 | 1991 |
|        2 | 1992 |
|        2 | 1994 |
|        2 | 1995 |
|        1 | 1996 |
|        1 | 1997 |
|        1 | 1998 |
|        4 | 1999 |
|        5 | 2000 |
|        3 | 2001 |
|        4 | 2003 |
|        2 | 2004 |
|        1 | 2005 |
+----------+------+`

6. How many movie titles have the word “the” in them?

SELECT 
  COUNT(*) 
FROM 
  imdb.movies 
WHERE 
  title LIKE '% the%' 
OR 
  title LIKE '%the %'


+----------+
| COUNT(*) |
+----------+
|        6 |
+----------+