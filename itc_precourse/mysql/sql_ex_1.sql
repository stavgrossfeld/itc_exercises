1. Which databases are managed by your server?
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| imdb               |
| mydb               |
| mysql              |
| performance_schema |
| sys                |
+--------------------+
6 rows in set (0.04 sec)



2. Which tables compose the imdb database?

mysql> show tables in imdb;
+----------------+
| Tables_in_imdb |
+----------------+
| actors         |
| cast           |
| kb1            |
| kb2            |
| kb3            |
| kb_4           |
| movie_actors   |
| movies         |
+----------------+
8 rows in set (0.00 sec)


3. What information can you find in the “movies” table? 

movie_id, title, year, genre

mysql> describe imdb.movies;
+-------+--------------+------+-----+---------+-------+
| Field | Type         | Null | Key | Default | Extra |
+-------+--------------+------+-----+---------+-------+
| id    | int(11)      | NO   | PRI | 0       |       |
| title | varchar(100) | YES  |     | NULL    |       |
| year  | int(11)      | YES  |     | NULL    |       |
| genre | varchar(100) | YES  |     | NULL    |       |
+-------+--------------+------+-----+---------+-------+
4 rows in set (0.05 sec)
