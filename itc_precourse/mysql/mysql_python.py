#pip install pymysql

import pandas as pd
import numpy as np

import matplotlib.pyplot as plt
import seaborn as sns
sns.set_style('white')
# %matplotlib inline


sns.set(rc={"figure.figsize": (12, 12)})
import pandas as pd
import pymysql
from sqlalchemy import create_engine

engine = create_engine('mysql+pymysql://root@localhost/imdb')

df = pd.read_sql_query('SELECT * FROM cast', engine)
df.head()


df = pd.read_sql_query('SELECT * FROM movies', engine)
df.head()


df = pd.read_sql_query('SELECT * FROM actors', engine)
df.head()



genre_year = pd.read_sql_query("select count(*) AS ct, genre, year  FROM movies WHERE genre = 'Drama' or genre = 'Comedy' or genre = 'Action' group by genre, year;", engine)
genre_year.head()

ax = sns.barplot(x="year", y="ct", hue="genre", data=genre_year); 


ax.set_ylabel('ct')
ax.set_title('Genre Distribution By Year')
plt.savefig('genre_year.png')

