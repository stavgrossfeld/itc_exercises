1. How many actors participated in the film “Aliens”?

use imdb;
DROP VIEW movie_actors;
CREATE VIEW movie_actors AS 
select 
    movie_id,
    title, 
    actor_id,
    full_name,
    salary
  from 
    imdb.actors 
  JOIN 
    imdb.cast 
  on 
    actors.id = cast.actor_id 
  JOIN 
    imdb.movies 
  ON 
    movies.id = cast.movie_id
;

select count(*) FROM movie_actors WHERE title = 'aliens';
+----------+
| count(*) |
+----------+
|       30 |
+----------+



2. Which films Kevin Bacon participated in?

select title from movie_actors WHERE full_name = 'Kevin Bacon';
+------------------------------+
| title                        |
+------------------------------+
| Animal House                 |
| Apollo 13                    |
| Few Good Men, A              |
| Footloose                    |
| Hollow Man                   |
| JFK                          |
| Mystic River                 |
| Planes, Trains & Automobiles |
| Stir of Echoes               |
+------------------------------+

3. Which actors have participated in a film with Kevin Bacon?

SELECT 
  DISTINCT(full_Name)
FROM 
  movie_actors 
WHERE
  title in (select title from movie_actors WHERE full_name = 'Kevin Bacon')

597 rows in set (0.01 sec)

4. Which actor has the highest average wage? The column salary is within the cast table (Hint :
you cannot use a function on a function)


SELECT 
  full_name,
  avg(salary) AS avg_sal
FROM 
  movie_actors 
GROUP BY 
  full_name
ORDER BY 
  avg_sal DESC LIMIT 1


+-------------+-------------+
| full_name   | avg_sal     |
+-------------+-------------+
| Grant James | 999435.0000 |
+-------------+-------------+