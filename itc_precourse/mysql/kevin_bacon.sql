DROP VIEW movie_actors;
CREATE VIEW movie_actors AS 
select 
    movie_id,
    actor_id,
    full_name
  from 
    actors 
  JOIN 
    cast 
  on 
    actors.id = cast.actor_id 
;





CREATE TABLE kb1 AS
SELECT 
  *
FROM 
  movie_actors
WHERE
  full_name = 'Kevin Bacon'


DROP TABLE kb2;
CREATE table kb2
AS
SELECT
  a.movie_id   AS movie_1,
  a.actor_id  AS actor_1,
  b.movie_id   AS movie_2,
  b.actor_id  AS actor_2
FROM 
  kb1 a
LEFT JOIN 
  movie_actors b
ON 
  a.movie_id = b.movie_id;


drop table kb3;

CREATE table kb3
AS
SELECT
  movie_1,
  actor_1,
  movie_2,
  actor_2,
  b.movie_id AS movie_3,
  c.actor_id AS actor_3
FROM 
  kb2 a
LEFT JOIN 
  movie_actors b
ON 
  a.actor_2 = b.actor_id
LEFT JOIN 
  movie_actors c
ON 
  b.movie_id = c.movie_id;




drop table kb_4;

CREATE table kb_4
AS
SELECT
  movie_1,
  actor_1,
  movie_2,
  actor_2,
  movie_3,
  actor_3,
  b.movie_id AS movie_4,
  c.actor_id AS actor_4
FROM 
  kb3 a
LEFT JOIN 
  movie_actors b
ON 
  a.actor_3 = b.actor_id
LEFT JOIN 
  movie_actors c
ON 
  b.movie_id = c.movie_id;


# count distincts, sql took too long to run my queries
SELECT
  COUNT(DISTINCT(actor_1)) AS ct_1,
  COUNT(DISTINCT(actor_2)) AS ct_2,
  COUNT(DISTINCT(actor_3)) AS ct_3,
  COUNT(DISTINCT(actor_4)) AS ct_4
FROM 
  kb_4





drop table kb_5;

CREATE table kb_5
AS
SELECT
  movie_1,
  actor_1,
  movie_2,
  actor_2,
  movie_3,
  actor_3,
  movie_4,
  actor_4,
  b.movie_id AS movie_5,
  c.actor_id AS actor_5
FROM 
  kb_4 a
LEFT JOIN 
  movie_actors b
ON 
  a.actor_4 = b.actor_id
LEFT JOIN 
  movie_actors c
ON 
  b.movie_id = c.movie_id;




drop table kb_6;

CREATE table kb_6
AS
SELECT
  movie_1,
  actor_1,
  movie_2,
  actor_2,
  movie_3,
  actor_3,
  movie_4,
  actor_4
  movie_5,
  actor_5,
  b.movie_id AS movie_6,
  c.actor_id AS actor_6
FROM
  kb_5 a
LEFT JOIN 
  movie_actors b
ON 
  a.actor_5 = b.actor_id
LEFT JOIN 
  movie_actors c
ON 
  b.movie_id = c.movie_id;



drop table kb_7;

CREATE table kb_7
AS
SELECT
  movie_1,
  actor_1,
  movie_2,
  actor_2,
  movie_3,
  actor_3,
  movie_4,
  actor_4
  movie_5,
  actor_5,
  movie_6,
  actor_6,
  b.movie_id AS movie_7,
  c.actor_id AS actor_7
FROM
  kb_6 a
LEFT JOIN 
  movie_actors b
ON 
  a.actor_6 = b.actor_id
LEFT JOIN 
  movie_actors c
ON 
  b.movie_id = c.movie_id;



