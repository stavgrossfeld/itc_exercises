use imdb;
DROP VIEW movie_actors;
CREATE VIEW movie_actors AS 
select 
    movie_id,
    title, 
    actor_id,
    full_name,
    salary,
    genre,
    year
  from 
    imdb.actors 
  JOIN 
    imdb.cast 
  on 
    actors.id = cast.actor_id 
  JOIN 
    imdb.movies 
  ON 
    movies.id = cast.movie_id
;


1. Who are the actors that have participated only in the films “Star Wars” and “Return of the
Jedi”?

SELECT 
  *
FROM 
  movie_actors
WHERE
  title = 'Star Wars' AND title = "Return of the Jedi"

0 actors played both


2. Find the top five actors with the most diverse career (i.e. participation in movies of different
genres).

SELECT 
  full_name,
  COUNT(DISTINCT(genre)) AS genre_ct
FROM 
  movie_actors
GROUP BY
  full_name
ORDER BY
  genre_ct desc
LIMIT 5


+----------------+----------+
| full_name      | genre_ct |
+----------------+----------+
| Kevin Bacon    |        6 |
| Brad Pitt      |        3 |
| Michael Madsen |        3 |
| Stevo Polyi    |        3 |
| Bill Paxton    |        3 |
+----------------+----------+

3. Which actor holds the record for participating in the most films in a single year?

SELECT 
  full_name,
  MAX(movie_ct_per_year) AS max_ct
FROM 
(
	SELECT 
	  full_name, 
	  year,
	  COUNT(*) AS movie_ct_per_year
	FROM 
	  movie_actors
	GROUP BY
	  full_name,
	  year
) movies_year
GROUP BY 
  full_name
ORDER BY
  max_ct
DESC LIMIT 1

+--------------+--------+
| full_name    | max_ct |
+--------------+--------+
| Cameron Diaz |      2 |
+--------------+--------+

4. What is the average career span (i.e. years between the first film and the last film) of actors
who participated in five films during their entire career?

SELECT 
  full_name, 
  MAX(year)  AS last,
  MIN(year)  AS first,
  COUNT(*)   AS film_ct
FROM 
  movie_actors
GROUP BY
  full_name
HAVING
  film_ct = 5

Empty set (0.01 sec)

5. Which actor had the longest hiatus between two of his films?

-- create index
set @row_num = 0; 
CREATE TABLE imdb.year_index AS 
SELECT 
  full_name,
  year,
  @row_num := @row_num + 1 AS my_index
FROM 
  movie_actors
ORDER BY
  full_name, year asc
-- 

SELECT
  full_name,
  MAX(hiatus) AS max_hiatus
FROM 
(
  SELECT
    full_name,
    y2_year - y1_year AS hiatus 
  FROM 
  (
  	SELECT 
  	  y1.full_name AS full_name,
  	  y1.year    y1_year,
  	  y2.year AS y2_year
  	FROM 
  	(
  		SELECT 
  		  full_name,
  		  year,
  		  my_index
  		FROM 
  		  imdb.year_index
  	) y1
  	JOIN 
  	(SELECT 
  		  full_name,
  		  year,
  		  my_index - 1 as my_index
  		FROM 
  		  imdb.year_index
  	) y2
  	ON 
  	  y1.my_index = y2.my_index
  	AND  
  	  y1.full_name = y2.full_name
  ) hiatus
) maxing
GROUP BY 
  full_name
ORDER BY 
  max_hiatus DESC LIMIT 1

+-----------------+------------+
| full_name       | max_hiatus |
+-----------------+------------+
| Malcolm Tierney |         18 |
+-----------------+------------+