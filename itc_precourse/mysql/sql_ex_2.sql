sql_ex_2.sql


create database mydb;

DROP TABLE mydb.food;
CREATE TABLE mydb.food (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(30),
do_i_like_it TINYINT(1)
);

INSERT INTO mydb.food
 (id, name, do_i_like_it)
VALUES
(DEFAULT, 'pizza', 1),
(DEFAULT , 'brocoli' , 1),
(DEFAULT , 'icecream' , 1);

update mydb.food 
	SET do_i_like_it = 0
WHERE name = 'brocoli' LIMIT 1;

DELETE FROM mydb.food WHERE name = 'brocoli' LIMIT 1;