#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void swap_integers(int *p1, int *p2);
void sort_integers(int a,  int b, int c);


int main(int argc, char **argv)
{
	char* end;
	int a = strtol(argv[1], &end, 10);
	int b = strtol(argv[2], &end, 10);
	int c = strtol(argv[3], &end, 10);
    sort_integers(a, b, c);
    return 0;
}
void sort_integers(int a, int b, int c)
{
	if (a < b) swap_integers(&a, &b);
	if (b < c) swap_integers(&b, &c);
	if (a < b) swap_integers(&a, &b);
	printf("%d %d %d \n", a, b, c);
}
void swap_integers(int *x, int *y)
{
	int temp;
	temp = *x;
	*x = *y;
	*y = temp;
}  
