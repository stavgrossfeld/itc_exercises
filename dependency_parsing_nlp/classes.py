import numpy as np
import time

from scipy.sparse import csr_matrix, vstack

from chu_liu import Digraph
from constants import DATA_PATH, MODELS_PATH, COMP_PATH, TEST_PATH


class Corpus:
    def __init__(self, file_name, insert_labels):

        if not file_name:
            raise Exception('Invalid file_name')

        self.sentences = []
        with open(DATA_PATH + file_name) as f:
            sentence = []
            for i, line in enumerate(f):
                if line == "\n":
                    self.sentences.append(Sentence(sentence=sentence, insert_labels=insert_labels))
                    sentence = []
                else:
                    sentence.append(line)

        self.sentences = np.array(self.sentences)

    def count_sentences(self):
        return np.size(self.sentences)

    def count_tokens(self):
        return sum([sen.count_tokens() for sen in self.sentences])


class Sentence:
    def __init__(self, sentence, insert_labels):
        self.tokens = [Token(line=token, insert_labels=insert_labels) for token in sentence]

    def graph(self):
        g = {
            0: [m.index for m in self.tokens]
        }
        for token_index, token in enumerate(self.tokens):
            g[token.index] = [m.index for m in self.tokens if m.index != token.index]
        return g

    def truth_tree(self):
        t = {i: [] for i in range(len(self.tokens) + 1)}
        for token in self.tokens:
            t[token.head].append(token.index)
        return t

    def compare_tree(self, tree_to_compare):
        return self.truth_tree() == tree_to_compare

    def argmax(self, w, features_matrix):
        scores = features_matrix.dot(w)
        return Digraph(self.graph(), lambda h, m: scores[self.score_index(h, m)]).mst().successors

    def global_feature_vector(self, features_matrix, tree=None):
        if tree is None:
            tree = self.truth_tree()
        global_matrix = []
        for h, modifiers in tree.items():
            for m in modifiers:
                global_matrix.append(features_matrix.getrow(self.score_index(h, m)))
        return np.array(vstack(global_matrix).sum(axis=0))[0]

    def score_index(self, h, m):
        return (m - 1) * self.length() + (h - 1) if h > m else (m - 1) * self.length() + h

    def length(self):
        return len(self.tokens)

    def get_token_head(self, token):
        return self.tokens[token.head - 1] if token.head >= 1 else Token.root_token()

    def get_head_word_and_tag(self, token):
        head = self.get_token_head(token)
        return head.word, head.tag

    @staticmethod
    def split_cleaned_line(line):
        return line.strip().split("\n")

    def count_tokens(self):
        return len(self.tokens)


class Token:
    def __init__(self, index=None, word=None, tag=None, head=None, line=None, insert_labels=True):
        self.index = index
        self.word = word
        self.tag = tag
        self.head = head

        if line is not None:
            self.split_line(line, insert_labels)

    def split_line(self, line, insert_labels):
        splitted_line = line.split("\t")
        if insert_labels:
            self.index, self.word, self.tag, self.head = \
                int(splitted_line[0]), splitted_line[1], splitted_line[3], int(splitted_line[6])
        else:
            self.index, self.word, self.tag = \
                int(splitted_line[0]), splitted_line[1], splitted_line[3]
            self.head = None

    @staticmethod
    def root_token():
        return Token(0, "ROOT", "ROOT")


class FeatureVector:
    def __init__(self, corpus):
        self.corpus = corpus

        self.features = {
            1: {},
            2: {},
            3: {},
            4: {},
            5: {},
            6: {},
            # 7: {},
            8: {},
            # 9: {},
            10: {},
            # 11: {},
            # 12: {},
            13: {}
            # TODO add more features
        }
        self.index = 0

    def increment_index(self):
        self.index += 1

    def f_1(self, head_word, head_tag):
        if (head_word, head_tag) not in self.features[1]:
            self.features[1][(head_word, head_tag)] = self.index
            self.increment_index()

    def f_2(self):
        pass  # TODO ...

    def initialize_features(self):
        for sentence in self.corpus.sentences:
            for index, token in enumerate(sentence.tokens):
                head_word, head_tag = sentence.get_head_word_and_tag(token)
                self.f_1(head_word, head_tag)

    def evaluate_feature_vector(self, sentence, token, head_token=None):
        if head_token is None:
            head_word, head_tag = sentence.get_head_word_and_tag(token)
        else:
            head_word, head_tag = head_token.word, head_token.tag
        return [elem for elem in [
            self.features[1].get((head_word, head_tag)),
            # TODO complete features
        ] if elem is not None]

    def print_num_of_features(self):
        for feature_type, features in self.features.items():
            print("f_{}: {}".format(feature_type, len(features)))

    def count_features(self):
        return self.index


class LocalFeatureVectors:

    def __init__(self):
        self.corpus = None
        self.feature_vector = None
        self.all_local_feature_vectors = []

    def evaluate_feature_vectors(self):
        start_time = time.time()

        for s_index, sentence in enumerate(self.corpus.sentences):
            print("evaluate for sentence: {}".format(s_index))
            col_indices = []
            row_indices = []
            row_index = 0
            for token_index, token in enumerate(sentence.tokens):
                root_head = Token.root_token()
                f = self.feature_vector.evaluate_feature_vector(sentence, token, root_head)
                col_indices += f
                row_indices += [row_index for _ in f]
                row_index += 1
                for head_index, token_head in enumerate(sentence.tokens):
                    if head_index != token_index:
                        f = self.feature_vector.evaluate_feature_vector(sentence, token, token_head)
                        col_indices += f
                        row_indices += [row_index for _ in f]
                        row_index += 1
            data = np.ones(len(col_indices))
            self.all_local_feature_vectors.append(csr_matrix(
                (data, (row_indices, col_indices)),
                shape=(row_index, self.feature_vector.count_features())))

        print("evaluate_feature_vectors done: {0:.3f} seconds".format(time.time() - start_time))


class Train(LocalFeatureVectors):

    def __init__(self, corpus, feature_vector):
        super().__init__()
        if not isinstance(corpus, Corpus):
            raise Exception('The corpus argument is not a Corpus object')
        self.corpus = corpus
        self.feature_vector = feature_vector
        self.w = None
        self.all_local_feature_vectors = []

    def generate_features(self):
        start_time = time.time()
        self.feature_vector.initialize_features()
        count_features = self.feature_vector.count_features()
        self.w = np.zeros(count_features)
        print("generate_feature done: {0:.3f} seconds".format(time.time() - start_time))
        print("Features count: {}".format(count_features))

    def perceptron(self, iterations):
        optimization_time = time.time()
        print("Perceptron")
        print("------------------------------------")
        # TODO complete the Perceptron algorithm:
        # for each iteration:
        #   for each sentence
        #       fix self.w according to the algorithm.
        # make sure to use: self.all_local_feature_vectors, sentence.argmax(...), sentence.compare_tree(...),
        # sentence.global_feature_vector(...)
        # The entire algorithm should not take you more than 10 lines of code

        for iteration in range(iterations):
            for sentence_ix, sentence in enumerate(self.corpus.sentences):
                print(sentence)
                # sentence.argmax(does the gen, )

                f_x = sentence.argmax(self.w, self.all_local_feature_vectors)

                if sentence.compare_tree(f_x) != True:
                    # w  = w + truth - predicted
                    self.w = self.w + self.global_feature_vector(self.all_local_feature_vectors) - self.global_feature_vector(self.all_local_feature_vectors, f_x)
                else:
                    continue


        self.save_model("{}_iterations".format(iterations))

        print("Total execution time: {0:.3f} seconds".format(time.time() - optimization_time))

    def save_model(self, model_name):
        np.savetxt(MODELS_PATH + model_name, self.w)


class Test(LocalFeatureVectors):

    def __init__(self, corpus, feature_vector, w=None):
        super().__init__()
        if not isinstance(corpus, Corpus):
            raise Exception('The corpus argument is not a Corpus object :(')
        self.corpus = corpus
        self.feature_vector = feature_vector
        self.w = w
        self.all_local_feature_vectors = []

    def inference(self):
        optimization_time = time.time()
        print("Inference")
        print("------------------------------------")

        # TODO complete the inference
        # for each sentence in self.corpus
        #   update the head of each token to the predicted one.
        # make sure to use self.all_local_feature_vectors, sentence.argmax(...)
        # The entire function should not take you more than 10 lines of code



        print("Total execution time: {0:.3f} seconds".format(time.time() - optimization_time))

    def load_model(self, model_name):
        self.w = np.loadtxt(MODELS_PATH + model_name)

    def evaluate_model(self, ground_truth):
        results = {
            "correct": 0,
            "errors": 0
        }
        for s1, s2 in zip(self.corpus.sentences, ground_truth.sentences):
            for t1, t2 in zip(s1.tokens, s2.tokens):
                if t1.head == t2.head:
                    results["correct"] += 1
                else:
                    results["errors"] += 1

        return results, results["correct"] / sum(results.values())

    def print_results_to_file(self, labeled_file, model_name, is_test):
        path = TEST_PATH if is_test else COMP_PATH

        f = open(path + model_name, 'w')
        for s_truth, s_eval in zip(labeled_file.sentences, self.corpus.sentences):
            f.write("".join(["{}\t{}\t_\t{}\t_\t_\t{}\t_\t_\t_\n".format(t_truth.index, t_truth.word, t_truth.tag,
                                                                        t_eval.head) for t_truth, t_eval
                              in zip(s_truth.tokens, s_eval.tokens)]) + "\n")

    def print_accuracy(self, model_name, accuracy):
        f = open(TEST_PATH + model_name + "_accuracy", 'w')
        f.write(accuracy)
        f.close()
