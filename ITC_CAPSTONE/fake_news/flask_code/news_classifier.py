# -*- coding: utf-8 -*-

# export FLASK_APP=news_classifier.py
#  flask run


from flask import Flask, request, render_template
import pandas as pd
import nltk
import joblib
from nltk.tokenize import sent_tokenize

from sklearn.tree import DecisionTreeClassifier
#from sklearn.cross_validation import train_test_split


text_clf = joblib.load("text_clf.pkl")

coefs = text_clf.steps[1][1].coef_[0] # get coefs
feature_names = text_clf.steps[0][1].get_feature_names() # get features
feature_imp = pd.DataFrame({"names":feature_names, "coef":coefs})

app = Flask(__name__)

# predict on any name code
def clean_text(text_data):
    """clean text from punctuation"""
    #text_data = text_data.encode("utf-8")

    for punc in "@#^*():,'/.1234567890%":
        text_data = text_data.replace(punc, "").replace("\n","")

    #text_data = text_data.encode('ascii',errors='ignore')
    return text_data

def highlight_sentences(sentences):
    # sentence_marks = []
    # sentences = sent_tokenize(sentences)
    # for sentence in sentences:
    #     sentence_mark = 0
    #     words = sentence.split(" ")
    #     for word in words:
    #         try:
    #             grade = feature_imp[feature_imp.names == word].coef.values[0]

    #             #print grade

    #             if grade > .2:
    #                 sentence_mark = 1
    #             if  grade > .5:
    #                 sentence_mark = 2
    #             if  grade > .8:
    #                 sentence_mark = 3
    #                 break
    #             else:
    #                 continue
    #         except:
    #             continue
    #     sentence_marks.append(sentence_mark)

    # return  ([(sentence, mark) for sentence, mark in zip(sentences, sentence_marks)]), sum(sentence_marks)
    return [['Daniel Greenfield, a Shillman Journalism Fellow at the Freedom Center, is a New York writer focusing on radical Islam.',
  0],
 ['In the final stretch of the election, Hillary Rodham Clinton has gone to war with the FBI.',
  0],
 ['The word “unprecedented” has been thrown around so often this election that it ought to be retired.',
  0],
 ['But it’s still unprecedented for the nominee of a major political party to go war with the FBI.',
  0],
 ['But that’s exactly what Hillary and her people have done.', 0],
 ['Coma patients just waking up now and watching an hour of CNN from their hospital beds would assume that FBI Director James Comey is Hillary’s opponent in this election.',
  0],
 ['The FBI is under attack by everyone from Obama to CNN.', 1],
 ['Hillary’s people have circulated a letter attacking Comey.', 0],
 ['There are currently more media hit pieces lambasting him than targeting Trump.',
  0],
 ['It wouldn’t be too surprising if the Clintons or their allies were to start running attack ads against the FBI.',
  1],
 ['The FBI’s leadership is being warned that the entire left-wing establishment will form a lynch mob if they continue going after Hillary.',
  1],
 ['And the FBI’s credibility is being attacked by the media and the Democrats to preemptively head off the results of the investigation of the Clinton Foundation and Hillary Clinton.',
  0],
 ['The covert struggle between FBI agents and Obama’s DOJ people has gone explosively public.',
  0],
 ['The New York Times has compared Comey to J. Edgar Hoover.', 2],
 ['Its bizarre headline, “James Comey Role Recalls Hoover’s FBI, Fairly or Not” practically admits up front that it’s spouting nonsense.',
  0],
 ['The Boston Globe has published a column calling for Comey’s resignation.',
  1],
 ['Not to be outdone, Time has an editorial claiming that the scandal is really an attack on all women.',
  2],
 ['James Carville appeared on MSNBC to remind everyone that he was still alive and insane.',
  0],
 ['He accused Comey of coordinating with House Republicans and the KGB.', 0],
 ['And you thought the “vast right wing conspiracy” was a stretch.', 1],
 ['Countless media stories charge Comey with violating procedure.', 0],
 ['Do you know what’s a procedural violation?', 0],
 ['Emailing classified information stored on your bathroom server.', 1],
 ['Senator Harry Reid has sent Comey a letter accusing him of violating the Hatch Act.',
  0],
 ['The Hatch Act is a nice idea that has as much relevance in the age of Obama as the Tenth Amendment.',
  0],
 ['But the cable news spectrum quickly filled with media hacks glancing at the Wikipedia article on the Hatch Act under the table while accusing the FBI director of one of the most awkward conspiracies against Hillary ever.',
  0],
 ['If James Comey is really out to hurt Hillary, he picked one hell of a strange way to do it.',
  1],
 ['Not too long ago Democrats were breathing a sigh of relief when he gave Hillary Clinton a pass in a prominent public statement.',
  1],
 ['If he really were out to elect Trump by keeping the email scandal going, why did he trash the investigation?',
  1],
 ['Was he on the payroll of House Republicans and the KGB back then and playing it coy or was it a sudden development where Vladimir Putin and Paul Ryan talked him into taking a look at Anthony Weiner’s computer?',
  0],
 ['Either Comey is the most cunning FBI director that ever lived or he’s just awkwardly trying to navigate a political mess that has trapped him between a DOJ leadership whose political futures are tied to Hillary’s victory and his own bureau whose apolitical agents just want to be allowed to do their jobs.',
  2],
 ['The only truly mysterious thing is why Hillary and her associates decided to go to war with a respected Federal agency.',
  1],
 ['Most Americans like the FBI while Hillary Clinton enjoys a 60% unfavorable rating.',
  0],
 ['And it’s an interesting question.', 0],
 ['Hillary’s old strategy was to lie and deny that the FBI even had a criminal investigation underway.',
  0],
 ['Instead her associates insisted that it was a security review.', 0],
 ['The FBI corrected her and she shrugged it off.', 0],
 ['But the old breezy denial approach has given way to a savage assault on the FBI.',
  2],
 ['Pretending that nothing was wrong was a bad strategy, but it was a better one that picking a fight with the FBI while lunatic Clinton associates try to claim that the FBI is really the KGB.',
  0],
 ['There are two possible explanations.', 0],
 ['Hillary Clinton might be arrogant enough to lash out at the FBI now that she believes that victory is near.',
  0],
 ['The same kind of hubris that led her to plan her victory fireworks display could lead her to declare a war on the FBI for irritating her during the final miles of her campaign.',
  1],
 ['But the other explanation is that her people panicked.', 0],
 ['Going to war with the FBI is not the behavior of a smart and focused presidential campaign.',
  1],
 ['It’s an act of desperation.', 0],
 ['When a presidential candidate decides that her only option is to try and destroy the credibility of the FBI, that’s not hubris, it’s fear of what the FBI might be about to reveal about her.',
  1],
 ['During the original FBI investigation, Hillary Clinton was confident that she could ride it out.',
  0],
 ['And she had good reason for believing that.', 0],
 ['But that Hillary Clinton is gone.', 0],
 ['In her place is a paranoid wreck.', 0],
 ['Within a short space of time the “positive” Clinton campaign promising to unite the country has been replaced by a desperate and flailing operation that has focused all its energy on fighting the FBI.',
  1],
 ['There’s only one reason for such bizarre behavior.', 0],
 ['The Clinton campaign has decided that an FBI investigation of the latest batch of emails poses a threat to its survival.',
  1],
 ['And so it’s gone all in on fighting the FBI.', 0],
 ['It’s an unprecedented step born of fear.', 0],
 ['It’s hard to know whether that fear is justified.', 1],
 ['But the existence of that fear already tells us a whole lot.', 1],
 ['Clinton loyalists rigged the old investigation.', 0],
 ['They knew the outcome ahead of time as well as they knew the debate questions.',
  1],
 ['Now suddenly they are no longer in control.', 0],
 ['And they are afraid.', 0],
 ['You can smell the fear.', 0],
 ['The FBI has wiretaps from the investigation of the Clinton Foundation.', 0],
 ['It’s finding new emails all the time.', 1],
 ['And Clintonworld panicked.', 0],
 ['The spinmeisters of Clintonworld have claimed that the email scandal is just so much smoke without fire.',
  0],
 ['All that’s here is the appearance of impropriety without any of the substance.',
  0],
 ['But this isn’t how you react to smoke.', 0],
 ['It’s how you respond to a fire.', 0],
 ['The misguided assault on the FBI tells us that Hillary Clinton and her allies are afraid of a revelation bigger than the fundamental illegality of her email setup.',
  0],
 ['The email setup was a preemptive cover up.', 0],
 ['The Clinton campaign has panicked badly out of the belief, right or wrong, that whatever crime the illegal setup was meant to cover up is at risk of being exposed.',
  1],
 ['The Clintons have weathered countless scandals over the years.', 1],
 ['Whatever they are protecting this time around is bigger than the usual corruption, bribery, sexual assaults and abuses of power that have followed them around throughout the years.',
  1],
 ['This is bigger and more damaging than any of the allegations that have already come out.',
  0],
 ['And they don’t want FBI investigators anywhere near it.', 1],
 ['The campaign against Comey is pure intimidation.', 1],
 ['It’s also a warning.', 0],
 ['Any senior FBI people who value their careers are being warned to stay away.',
  1],
 ['The Democrats are closing ranks around their nominee against the FBI.', 0],
 ['It’s an ugly and unprecedented scene.', 0],
 ['It may also be their last stand.', 0],
 ['Hillary Clinton has awkwardly wound her way through numerous scandals in just this election cycle.',
  1],
 ['But she’s never shown fear or desperation before.', 1],
 ['Now that has changed.', 0],
 ['Whatever she is afraid of, it lies buried in her emails with Huma Abedin.',
  1],
 ['And it can bring her down like nothing else has.', 0]]

app = Flask(__name__)

@app.route('/', methods = ["GET","POST"])
def serve_template():
  if request.method == 'GET':
    return render_template('form.html')
  else:
    text = request.form.get('news_article')

    doc_clean = clean_text(text)
    prob_classes = text_clf.predict_proba([doc_clean])[0]
    prob_dict = pd.DataFrame(pd.Series({"FAKE":prob_classes[0],"REAL":prob_classes[1]}), columns = ["probability"])

    #text = text.decode("utf-8")

    sentence_marks = highlight_sentences(text)#, sentence_sum = highlight_sentences(text)#.encode("ascii","ignore"))
    sentence_marks = [[sent[0].decode("utf-8"), sent[1]] for sent in sentence_marks]
    print sentence_marks
    return render_template('colored.html', sentences=sentence_marks)
    #return prob_dict.to_html()





# @app.route("/fake_news/<message>")
# def gender(message):
#     clf = model
#     baby_name = str(message)
#     baby_names = baby_name.replace("+"," ").replace("'","").replace("%2C"," ").split(" ")
#     predicted_list = [(name, predict_name(name.lower(), clf)) for name in baby_names]
#     #print predicted_list
#     pred_df = pd.DataFrame([pred for pred in predicted_list])
#     pred_df.columns = ["name","is_girl"]
#     return pred_df.to_html()


if __name__ == "__main__":
  app.run()
