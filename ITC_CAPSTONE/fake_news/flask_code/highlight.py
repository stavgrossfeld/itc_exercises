
# -*- coding: utf-8 -*-
from nltk import sent_tokenize
import joblib
import pandas as pd

text_clf = joblib.load("text_clf.pkl")

coefs = text_clf.steps[1][1].coef_[0] # get coefs
feature_names = text_clf.steps[0][1].get_feature_names() # get features
feature_imp = pd.DataFrame({"names":feature_names, "coef":coefs})

def highlight_sentences(sentences):
    sentence_marks = []
    sentences = sent_tokenize(sentences)
    for sentence in sentences:
        sentence_mark = 0
        words = sentence.split(" ")
        for word in words:
            try:
                grade = feature_imp[feature_imp.names == word].coef.values[0]

                #print grade

                if grade > .2:
                    sentence_mark = 1
                if  grade > .5:
                    sentence_mark = 2
                if  grade > .8:
                    sentence_mark = 3
                    break
                else:
                    continue
            except:
                continue
        sentence_marks.append(sentence_mark)

    print ([(sentence, mark) for sentence, mark in zip(sentences, sentence_marks)]), sum(sentence_marks)

text = """
A Super Blue Blood Moon is coming
A rare celestial occurrence called a 'Super Blue Blood Moon' will be visible on January 31, 2018. What exactly is this rare phenomenon and where can you see it?

Skywatchers are being treated to the extremely rare phenomenon of a super blue blood moon early Wednesday.

The unusual lunar trifecta is occurring for the first time in North America since 1866, according to Space.com.

SUPER BLUE BLOOD MOON 2018: WHAT, WHEN AND WHERE


Our first view of the #SuperBlueBloodMoon is brought to you by our @NASAArmstrong Flight Research Center! Tune in live here: https://t.co/r6X6SoMfLn pic.twitter.com/q6JEuMoZ67

— NASA (@NASA) January 31, 2018
“The Jan. 31 full moon is special for three reasons: it’s the third in a series of ‘supermoons,’ when the Moon is closer to Earth in its orbit -- known as perigee -- and about 14 percent brighter than usual,” explains NASA. “It’s also the second full moon of the month, commonly known as a ‘blue moon.’ The super blue moon will pass through Earth’s shadow to give viewers in the right location a total lunar eclipse. While the Moon is in the Earth’s shadow it will take on a reddish tint, known as a ‘blood moon.’

Will you be able to see the #SuperBlueBloodMoon in your area? Check out this map to find out! For the continental U.S., viewing will be best on the West Coast. Get the details: https://t.co/ooerjToxKR
Not great viewing in your area? Watch our livestream https://t.co/6wIIyicomc pic.twitter.com/scD4UzkVkf

— NASA (@NASA) January 30, 2018
“For the (continental) U.S., the viewing will be best in the West,” explained Gordon Johnston, program executive and lunar blogger at NASA Headquarters in Washington, in a statement.

Across the globe, skywatchers have been closely monitoring the moon to see the rare event.

People set up telescopes on the waterfront for the super blue moon and eclipse in Hong Kong, China January 31, 2018. REUTERS/Bobby Yip - RC16F8115BB0
People set up telescopes on the waterfront for the super blue moon and eclipse in Hong Kong, China Jan. 31, 2018. (REUTERS/Bobby Yip)

Photographers have also been capturing the phenomenon on camera.

The Statue of Liberty is backdropped by a supermoon, Wednesday, Jan. 31, 2018, seen from the Brooklyn borough of New York. The supermoon, which is the final of three consecutive supermoons, also experience lunar eclipse as it set over the horizon, but only a partial eclipse was visible in the East Coast. (AP Photo/Julio Cortez)
The Statue of Liberty is backdropped by a supermoon, Wednesday, Jan. 31, 2018, seen from the Brooklyn borough of New York. The supermoon, which is the final of three consecutive supermoons, also experience lunar eclipse as it set over the horizon, but only a partial eclipse was visible in the East Coast. (AP Photo/Julio Cortez)

Only a partial eclipse, however, was visible in the East Coast of the U.S.

The Statue of Liberty is backdropped by a supermoon, Wednesday, Jan. 31, 2018, seen from the Brooklyn borough of New York. The supermoon, which is the final of three consecutive supermoons, also experience lunar eclipse as it set over the horizon, but only a partial eclipse was visible in the East Coast. (AP Photo/Julio Cortez)
The Statue of Liberty is backdropped by a supermoon, Wednesday, Jan. 31, 2018, seen from the Brooklyn borough of New York. (AP Photo/Julio Cortez)

While the super blue blood moon is visible in the pre-dawn hours in much of the U.S., other parts of the world viewed the lunar event in the middle of the night.

A full moon is seen during a lunar eclipse in Jakarta, Indonesia January 31, 2018. REUTERS/Darren Whiteside - RC195D982040
A full moon is seen during a lunar eclipse in Jakarta, Indonesia Jan. 31, 2018. (REUTERS/Darren Whiteside)

The lunar spectactle is generating plenty of buzz on social media, with #SuperBlueBloodMoon a topic trending global topic on Twitter.

Watching the #SuperBlueBloodMoon !

— Oprah Winfrey (@Oprah) January 31, 2018
The #superbluebloodmoon from Melbourne. pic.twitter.com/1xKOxbnmqi

— City of Melbourne (@cityofmelbourne) January 31, 2018
LOOK: A blue moon is seen over the Mayon volcano crater in Ligao, Albay province at around 7 p.m. on Wednesday. #SuperBlueBloodMoon pic.twitter.com/c0BFCqmjm6

— The Philippine Star (@PhilippineStar) January 31, 2018
A commercial plane passes in front of a #SuperBlueBloodMoon as seen from Muntinlupa on Wednesday. pic.twitter.com/Hc4pNKlkje

— The Philippine Star (@PhilippineStar) January 31, 2018
Some Twitter users, however, were underwhelmed by the spectacle.

Expectation VS Reality #SuperBlueBloodMoon pic.twitter.com/4c8Kzqc9B6

— Emille Alejandro (@Emilliooooo) January 31, 2018
A NASA live feed of the event can be viewed here.

The unusual lunar showstopper won't happen again until 2037.

The Associated Press contributed to this article.

Follow James Rogers on Twitter @jamesjrogers


Trending in Science
 Dust storms that take over the Martian globe play a key role in promoting gas escape from the Red Planet's atmosphere , according to a new study.
Escape from Mars! Red-planet dust storms linked to atmosphere loss
 Skywatchers are being treated to the extremely rare phenomenon of a super blue blood moon early Wednesday.
Super blue blood moon delights skywatchers
 When Veterinary nurse Brittany Semeniuk went to Canada last November, she and her vet partner photographed an unusual encounter between a massive polar bear and a chained sled dog.
Polar bear and sled dog snuggle in unlikely interaction
 People from across the country are gazing up into the sky early Wednesday morning to get a glimpse of the rare Super Blue Blood Moon.
Super Blue Blood Moon 2018: What, when and where



"""

text = text.decode("utf-8")
highlight_sentences(text)