import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.pipeline import make_pipeline

# import data
datacamp_news = pd.read_csv("../data/data_camp_news_fake_or_real.csv")
del datacamp_news["Unnamed: 0"]
datacamp_news.head()
datacamp_news = datacamp_news[datacamp_news.text != " "]


# clean text
def clean_text(text_data):
    """clean text from punctuation"""
    text_data = text_data.decode("utf-8")

    for punc in "@#^*():,'/.":
        text_data = text_data.replace(punc, "").replace("\n","")

    text_data = text_data.encode('ascii',errors='ignore')
    return text_data


doc_clean = datacamp_news.text
doc_clean = doc_clean.apply(lambda val: clean_text(val))


from sklearn.cross_validation import train_test_split
X_train, X_test,y_train, y_test = train_test_split(doc_clean.values, datacamp_news.label, test_size = .33)


def model(X_train, X_test, y_train, y_test):
    no_features = 1000
    # Initialize the `count_vectorizer`
    count_vectorizer = CountVectorizer(stop_words='english')
    # Fit and transform the training data
    count_train = count_vectorizer.fit_transform(X_train)
    # Transform the test set
    count_test = count_vectorizer.transform(X_test)

    clf = LogisticRegression()

    clf.fit(count_train, y_train)
    pred = clf.predict(count_test)
    score = metrics.accuracy_score(y_test, pred)

    print("accuracy:   %0.3f" % score)
    cr = metrics.classification_report(y_test, pred, labels=['FAKE', 'REAL'])
    print (cr)

    return count_vectorizer, clf

count_vec, clf = model(X_train, X_test, y_train, y_test)



print(X_test[1500])
indiv_text = [X_test[1500]]
#indiv_text = ["Donald Trump, who has routinely peddled conspiracy theories and mistruths from the office of the presidency, sought to question the accuracy of the media on Wednesday by unveiling the so-called Fake News Awards."]

def predict_individual(count_vec, clf, indiv_text):
    count_indiv = count_vec.transform(indiv_text)

    prob_classes = clf.predict_proba(count_indiv)[0]

    prob_dict = pd.DataFrame(pd.Series({"FAKE":prob_classes[0],"REAL":prob_classes[1]}), columns = ["probability"])

    print(prob_dict)
predict_individual(count_vec, clf, indiv_text)




# import pickle

# filename = 'fake_news_classifier.pickle'
# pickle.dump(model, open(filename, 'wb'))