# run in python3 news_api.py

import newspaper
from newsplease import NewsPlease
import pandas as pd



news_site_dict = {"google":"https://news.google.com/news/?ned=us&gl=US&hl=en",
"cnn":"http://edition.cnn.com/",
"newyorktimes":"https://www.nytimes.com/",
"thewashingtonpost": "https://www.washingtonpost.com/",
 "haaretz":"https://www.haaretz.com/",
 "theeconomist":"https://www.economist.com/",
 "nbc":"https://www.nbcnews.com/",
 "bbc":"http://www.bbc.com/news",
  "abc":"http://abcnews.go.com/",
 "aljazeera_english":"https://www.aljazeera.com/",
"breitbart":"http://www.breitbart.com/",
"thegaurdian":"https://www.theguardian.com/international",
"huffington":"http://www.huffingtonpost.com.au/",
"theslate":"https://slate.com/"}

article_objects = {}
for site in news_site_dict:
    article_objects[site] = newspaper.build(news_site_dict[site]).articles


# print length of articles
for i in article_objects:
    print (i,len(article_objects[i]))

# download article texts into dataframe


import time

ct = 0

article_df = pd.DataFrame(columns=['article_urls','article_text','article_title','article_source_url','article_label'])

def download_obs(article, news_label):
    article.download()
    article.parse()

    text = article.text
    url = article.url
    title = article.title
    source_url = article.source_url
    article_dict = {
        "article_text":text,
        "article_urls":url,
        "article_title":title,
        "article_source_url":source_url,
        "article_label":news_label}
    return article_dict


for news_label in article_objects:
    for article in article_objects[news_label]:
        ct += 1
        if ct % 5 == 0:
            print (ct)
        else:
            continue
        try:
            article_dict = download_obs(article, news_label)
        except:
            time.sleep(.5)
            print("sleep")
            try:
                article_dict = download_obs(article, news_label)
            except:
                continue
        article_df = article_df.append(article_dict, ignore_index=True)


article_df = article_df.drop_duplicates()

# filter out google default news

google_news_default = 'The selection and placement of stories on this page were determined automatically by a computer program.\n\nThe time or date displayed reflects when an article was added to or updated in Google News.'
article_df = article_df[article_df.article_text != google_news_default]

article_df = article_df[article_df.text.apply(lambda val: type(val)) == str]

article_df.to_csv("../data/api_news.csv", index = False)