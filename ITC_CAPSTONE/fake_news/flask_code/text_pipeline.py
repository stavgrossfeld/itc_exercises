import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn import metrics
from sklearn.cross_validation import train_test_split
import joblib


# import data
datacamp_news = pd.read_csv("../data/data_camp_news_fake_or_real.csv")
del datacamp_news["Unnamed: 0"]
datacamp_news.head()
datacamp_news = datacamp_news[datacamp_news.text != " "]


# clean text
def clean_text(text_data):
    """clean text from punctuation"""
    text_data = text_data.decode("utf-8")

    for punc in "@#^*():,'/.":
        text_data = text_data.replace(punc, "").replace("\n","")

    text_data = text_data.encode('ascii',errors='ignore')
    return text_data


doc_clean = datacamp_news.text
doc_clean = doc_clean.apply(lambda val: clean_text(val))


X_train, X_test,y_train, y_test = train_test_split(doc_clean.values, datacamp_news.label, test_size = .33)


def classify(X_train, X_test,y_train, y_test):
	from sklearn.pipeline import Pipeline
	text_clf = Pipeline([('vect', CountVectorizer()),
	                     ('clf', LogisticRegression())])

	text_clf.fit(X_train, y_train)

	y_pred = text_clf.predict(X_test)
	score = metrics.accuracy_score(y_test, y_pred)
	print("accuracy:   %0.3f" % score)
	cr = metrics.classification_report(y_test, y_pred, labels=['FAKE', 'REAL'])
	print (cr)

	return text_clf

text_clf = classify(X_train, X_test,y_train, y_test)

joblib.dump(text_clf, 'text_clf.pkl', compress = 1)

feature_names = text_clf.steps[0][1].get_feature_names()
feature_imp = pd.DataFrame(text_clf.steps[0][1].coef_,feature_names)
feature_imp.columns = ["coef"]

def pred_today():

  today_news = pd.read_csv("../data/api_news.csv")
  today_news.text = today_news.text.apply(lambda val: clean_text(val))

  prob_classes = text_clf.predict_proba(today_news.text.values)

  today_pred = ([np.argmax(i) for i in prob_classes])

  today_news_df_probs = today_news.copy()

  today_news_df_probs["fake_prob"] = pd.Series(prob_classes[:,0])
  today_news_df_probs["real_prob"] = pd.Series(prob_classes[:,1])
  today_news_df_probs["prediction"] =  pd.Series([np.argmax(i) for i in prob_classes])



  return today_news_df_probs

today_news_df_probs = pred_today()

today_news_df_probs.to_csv("./pred_data/todays_news_df_probs.csv", index = False)


# def predict_todays():

#   today_news = pd.read_csv("../data/api_news.csv")
#   today_news.text = today_news.text.apply(lambda val: clean_text(val))

#   prob_classes = text_clf.predict_proba(today_news.text.values)[0]

#   today_news_df_probs = pd.DataFrame(pd.Series({"FAKE":prob_classes[0],"REAL":prob_classes[1]}), columns = ["probability"])

#   print (today_news_df)